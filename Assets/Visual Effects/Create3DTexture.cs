﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create3DTexture : MonoBehaviour
{
    [ExecuteInEditMode]

    public Texture3D texture;

    public Texture3D texture2;

    VFXParameterEditor param;

    public ParticleSystem graph;
    public float constantValue = 1.0f;
    void Awake ()
    {
        texture = CreateTexture3D (64);
        
        param = this.GetComponent<VFXParameterEditor>();
    }

    Texture3D CreateTexture3D (int size)
    {
        Color[] colorArray = new Color[size * size * size];
        texture = new Texture3D (size, size, size, TextureFormat.RGBA32, true);
        float r = 1.0f / (size - 1.0f);
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                for (int z = 0; z < size; z++) {
                    Color c = new Color (0 * r, Mathf.Sin((1/constantValue) * y) * r, constantValue * r, 1.0f);
                    colorArray[x + (y * size) + (z * size * size)] = c;
                }
            }
        }
        
        Debug.Log("Executing ..."); 
        texture.SetPixels (colorArray);
        texture.Apply ();
        
        

        texture2.SetPixels(colorArray);
        texture2.Apply();
        return texture;
        
    }

   

}
