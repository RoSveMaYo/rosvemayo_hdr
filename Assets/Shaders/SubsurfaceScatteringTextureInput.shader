﻿Shader "Custom/SubsurfaceScatteringInput" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_SSColor("SSColor" , Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.0
		_Normal ("Normal" , 2D) = "bump" {}
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Distortion ("Distortion", Range(0,1)) = 0.0
		_Thickness ("Thickness" , 2D) = "white"{}
		_Emission ("Emissive" , 2D) = "white" {}
		_EmissiveStrength ("Emissive Strength" , Range(0,10)) = 1.0
		_Power ("Power", Range(0,5)) = 0.0
		_Scale ("Scale", Range(0,5)) = 0.0
		_Attenuation ("Attenuation", Range(0,5)) = 1.0
		_Ambient ("Ambient", Range(0,5)) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf StandardTranslucent fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Thickness;
		sampler2D _Normal;
		//sampler2D _Metallic;
		//sampler2D _Glossiness;
		sampler2D _Emission;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Glossiness;
			//float2 uv_Metallic;
			float2 uv_Normal;
			float2 uv_Emission;
		};

		fixed4 _Color;
		float _Distortion;
		fixed4 thickness;
		fixed4 _SSColor;
		half _Glossiness;
		half _Metallic;
		half _Power;
		half _Scale;
		half _Attenuation;
		half _Ambient;
		half _EmissiveStrength;
		#include "UnityPBSLighting.cginc"

		inline fixed4 LightingStandardTranslucent (SurfaceOutputStandard s , fixed3 viewDir, UnityGI gi )
		{
			fixed4 pbr = LightingStandard(s , viewDir , gi);

			float3 L = gi.light.dir;
 			float3 V = viewDir;
 			float3 N = s.Normal;
 
 			float3 H = normalize(L + N * _Distortion);
			 //power and scale
 			float VdotH = pow(saturate(dot(V, -H)), _Power) * _Scale;
			float3 I = _Attenuation * (VdotH + _Ambient) * thickness;

			pbr.rgb = (pbr.rgb + gi.light.color * I) * _SSColor;
			
			return pbr;
		}

		void LightingStandardTranslucent_GI(SurfaceOutputStandard s, UnityGIInput data, inout UnityGI gi)
		{
 			LightingStandard_GI(s, data, gi); 
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
			o.Normal = UnpackNormal (tex2D (_Normal, IN.uv_Normal));
			o.Emission = tex2D ( _Emission , IN.uv_Emission)* _EmissiveStrength;
			thickness = tex2D (_Thickness , IN.uv_MainTex);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
