﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ButtonBehaviour))]
[CanEditMultipleObjects]

public class MenuButtonEditor : Editor
{
    ButtonBehaviour myButtonBehaviour;

    private void OnEnable()
    {
        myButtonBehaviour = (ButtonBehaviour)target;
    }

    public override void OnInspectorGUI()
    {
        myButtonBehaviour.selectedFunction = (ButtonBehaviour.buttonFunction)EditorGUILayout.EnumPopup("Button Function", myButtonBehaviour.selectedFunction);
    }
}
