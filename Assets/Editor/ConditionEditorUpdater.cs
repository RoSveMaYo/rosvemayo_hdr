﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*
[CustomEditor(typeof(LoseConditions))]
[CanEditMultipleObjects]

public class ConditionEditorUpdater : Editor
{
    public override void OnInspectorGUI()
    {
        LoseConditions myLoseConditions = (LoseConditions)target;
        myLoseConditions.loseCondition = (LoseConditions.Conditions)EditorGUILayout.EnumPopup("Lose Condition", myLoseConditions.loseCondition);

        switch (myLoseConditions.loseCondition)
        {
            case LoseConditions.Conditions.UnderWater:
                Debug.Log("Show UnderWater Variables");
                myLoseConditions.coverPercentage = EditorGUILayout.FloatField("Cover Percentage", myLoseConditions.coverPercentage);
                break;

            case LoseConditions.Conditions.Timer:
                Debug.Log("Show Timer Variables");
                myLoseConditions.timer = EditorGUILayout.FloatField("Timer", myLoseConditions.timer);
                break;

            case LoseConditions.Conditions.FlipOver:
                Debug.Log("Show FlipOver Variables");
                myLoseConditions.maxAngle = EditorGUILayout.FloatField("Maximum Angle", myLoseConditions.maxAngle);
                break;

            default:
                Debug.Log("No Condition");
                break;
        }
    }
}
*/
