﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathHolder : MonoBehaviour
{
    public static PathHolder instance;

    public bool pathEnabled = true;

    public List<Transform> pathObjects = new List<Transform>();
    public int currentWaypointIndex;

    public Vector3 colliderSize = new Vector3(3, 200, 200);

    Transform[] objectArray;
    public Color rayColor = Color.white;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        currentWaypointIndex = 1;
        Debug.Log("PathObject Count: " + pathObjects.Count);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = rayColor;
        objectArray = GetComponentsInChildren<Transform>();
        pathObjects.Clear();

        foreach(Transform t in objectArray)
        {
            if(t != this.transform)
            {
                pathObjects.Add(t);

                if (t != this.transform.GetChild(0))
                {
                    if (t.gameObject.GetComponent<BoxCollider>() == null)
                    {
                        BoxCollider bc = t.gameObject.AddComponent<BoxCollider>();
                        bc.size = colliderSize;
                        bc.isTrigger = true;
                    }

                    if (t.gameObject.GetComponent<WaypointTrigger>() == null)
                    {
                        t.gameObject.AddComponent<WaypointTrigger>();
                    }
                }
            }

            else
            {
                continue;
            }
        }

        for(int i = 0; i < pathObjects.Count; i++)
        {
            Vector3 position = pathObjects[i].position;

            if(i > 0)
            {
                Vector3 previous = pathObjects[i - 1].position;
                Gizmos.DrawLine(previous, position);
            }
        }
    }
}
