﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Check if its an object to transport.
        if (other.gameObject.layer == 12 && GameStateManager.instance.gameState == GameStateManager.States.Playing) //That means object layer
        {
            PathHolder.instance.currentWaypointIndex++;
        }
    }
}
