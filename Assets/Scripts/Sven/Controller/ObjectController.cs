﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
    Rigidbody rb;
    public Transform gooContainer;

	// Use this for initialization
	void Start ()
    {
        if(InputManager.instance != null)
        {
            InputManager.instance.dropObjectDelegate += DropObjectIntoWater;
            InputManager.instance.cleanUpDelegate += CleanUpGoos;
        }

        else
        {
            Debug.Log("ObjectController cant find INPUTMANAGER");
        }

        rb = this.gameObject.GetComponent<Rigidbody>();
	}

    private void OnDisable()
    {
        InputManager.instance.dropObjectDelegate -= DropObjectIntoWater;
        InputManager.instance.cleanUpDelegate -= CleanUpGoos;
    }


    void CleanUpGoos()
    {
        foreach (Transform child in gooContainer)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    void DropObjectIntoWater()
    {
        rb.isKinematic = false;
    }
}
