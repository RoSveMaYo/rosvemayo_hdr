﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControllerScript : MonoBehaviour
{
    public GameObject currentGameObject;
    public GameObject player;
    public GameObject goalObject;

    WinTrigger winTrig;

    int selectedObject = 0;
    int previousSelectedObject;

	// Use this for initialization
	void Awake ()
    {
        SelectObject();
	}

    private void Start()
    {
        if(InputManager.instance != null)
        {
            InputManager.instance.objectSwitchDelegate += Switch;
            InputManager.instance.dropObjectDelegate += DropObject;
            InputManager.instance.dropObjectDelegate += CombineStoneMeshes;
            InputManager.instance.cleanUpDelegate += CleanGoos;
            InputManager.instance.stopCleaningDelegate += StopCleaning;
        }

        else
        {
            Debug.Log("ObjectSwitch cant find INPUTMANAGER");
        }

        winTrig = goalObject.GetComponent<WinTrigger>();
        winTrig.currentObjectName = currentGameObject.transform.name;
    }

    private void OnDisable()
    {
        InputManager.instance.objectSwitchDelegate -= Switch;
        InputManager.instance.dropObjectDelegate -= DropObject;
        InputManager.instance.dropObjectDelegate -= CombineStoneMeshes;
        InputManager.instance.cleanUpDelegate -= CleanGoos;
        InputManager.instance.stopCleaningDelegate -= StopCleaning;
    }

    void SelectObject()
    {
        int i = 0;
        foreach(Transform childObject in transform)
        {
            if(i == selectedObject)
            {
                childObject.gameObject.SetActive(true);
                currentGameObject = childObject.gameObject;
                player.GetComponent<DampCameraJitter>().target = currentGameObject.transform;
            }

            else
            {
                childObject.gameObject.SetActive(false);
            }

            i++;
        }
    }

    void SetSelected()
    {
        if (selectedObject >= transform.childCount - 1)
        {
            selectedObject = 0;
        }

        else
        {
            selectedObject++;
        }
        

        if (previousSelectedObject != selectedObject)
        {
            SelectObject();
        }
    }

    void Switch()
    {
        previousSelectedObject = selectedObject;
        SetSelected();
        winTrig.currentObjectName = currentGameObject.transform.name;
    }

    void DropObject()
    {
        currentGameObject.GetComponent<Rigidbody>().isKinematic = false;
    }

    void CleanGoos()
    {
        foreach(Transform child in currentGameObject.transform.GetChild(0))
        {
            child.gameObject.GetComponent<DeleteBehaviour>().BlowUp();
            //GameObject.Destroy(child.gameObject);
        }
    }

    void StopCleaning()
    {
        foreach (Transform child in currentGameObject.transform.GetChild(0))
        {
            child.gameObject.GetComponent<DeleteBehaviour>().Shrink();
            //GameObject.Destroy(child.gameObject);
        }
    }

    void CombineStoneMeshes()
    {
        currentGameObject.transform.GetChild(0).gameObject.GetComponent<StoneMeshCombiner>().CombineStoneMeshes();
    }
}
