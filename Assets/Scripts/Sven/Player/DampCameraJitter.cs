﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DampCameraJitter : MonoBehaviour
{
    public Transform target;

    Quaternion desiredRotation;
    Vector3 desiredPosition;

    //public float posSmoothing = 0.125f;
    //public float rotSmoothing = 0.125f;

    private void Update()
    {
        //desiredRotation = target.rotation;
        desiredPosition = target.position;
        //Vector3 smoothedPosition = Vector3.Lerp(this.transform.position, desiredPosition, posSmoothing);
        this.transform.position = target.position;
    }

    /*
    private void LateUpdate()
    {
        Vector3 smoothedPosition = Vector3.Lerp(this.transform.position, desiredPosition, posSmoothing);
        Quaternion smoothedRotation = Quaternion.Lerp(this.transform.rotation, desiredRotation, rotSmoothing);
        this.transform.position = smoothedPosition;
        this.transform.rotation = smoothedRotation;
    }
    */
}
