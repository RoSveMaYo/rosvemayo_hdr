﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteBehaviour : MonoBehaviour
{
    [HideInInspector]
    public Vector3 startScale;

    [HideInInspector]
    public bool canDelete;

    public ParticleSystem onDelete;

    public float timer = 1f;
    public float shrinkDuration = 0.5f;

    float startValue;
    [HideInInspector]
    public float endScale;

    bool count;
    bool isShrinking;

    MaterialBehaviour mBehaviour;

    private void Start()
    {
        mBehaviour = this.gameObject.GetComponent<MaterialBehaviour>();
        startValue = timer;
    }

    public void BlowUp()
    {
        count = true;
        TimeToBlowUp();
    }

    void TimeToBlowUp()
    {   
        if(!isShrinking)
        {
            if (count && timer > 0.0f)
            {
                timer -= Time.deltaTime;
                this.transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
            }

            else if (timer <= 0.0f)
            {
                count = false;
                timer = startValue;
                InstantiateParticleSystem();
                Destroy(this.gameObject);
            }
        }
    }

    public void Shrink()
    {
        isShrinking = true;
        count = false;
        timer = startValue;
        StartCoroutine(IShrink(this.transform.localScale, startScale, shrinkDuration));
    }

    IEnumerator IShrink(Vector3 start, Vector3 end, float time)
    {
        float i = 0f;
        float rate = (1f / time) * 1f;
        while (i < 1f)
        {
            i += Time.deltaTime * rate;
            this.transform.localScale = Vector3.Lerp(start, end, i);
            yield return null;
        }

        isShrinking = false;
    }

    void InstantiateParticleSystem()
    {
        onDelete.GetComponent<ParticleSystemRenderer>().material = mBehaviour.endMaterial;
        ParticleSystem ps = Instantiate(onDelete, this.gameObject.transform.position, Quaternion.identity);
    }
}
