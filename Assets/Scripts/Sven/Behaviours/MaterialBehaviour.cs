﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialBehaviour : MonoBehaviour, ICollision
{
    public Material startMaterial;
    public Material endMaterial;
    public float lerpDuration;

    public List<Material> MaterialList;
    public bool randomMaterial;

    public ParticleSystem onInstantiate;

    Renderer rend;

    private void Start()
    {
        SetStartMaterial();
    }

    public void DoBehaviour()
    {
        if(randomMaterial)
        {
            this.SetEndMaterial();
        }

        InstantiateParticleSystem(onInstantiate, this.gameObject.transform.position);
        StartExpand();
    }

    void SetStartMaterial()
    {
        rend = this.GetComponent<Renderer>();
        rend.material = startMaterial;
    }

    IEnumerator IMaterialLerp(Material start, Material end, float time)
    {
        float i = 0f;
        float rate = (1f / time) * 1f;
        while (i < 1f)
        {
            i += Time.deltaTime * rate;
            rend.material.Lerp(start, end, i);
            yield return null;
        }
    }

    void StartExpand()
    {
        StartCoroutine(IMaterialLerp(startMaterial, endMaterial, lerpDuration));
    }

    void SetEndMaterial()
    {
        int randomColorIndex = Random.Range(0,MaterialList.Count);
        Debug.Log(MaterialList.Count);

        endMaterial = MaterialList[randomColorIndex];
    }

    void InstantiateParticleSystem(ParticleSystem particleS, Vector3 position)
    {
        particleS.GetComponent<ParticleSystemRenderer>().material = endMaterial;
        particleS.transform.GetChild(0).GetComponent<ParticleSystemRenderer>().material = endMaterial;

        ParticleSystem ps = Instantiate(particleS, position, Quaternion.identity);
        ps.Play();
    }
}
