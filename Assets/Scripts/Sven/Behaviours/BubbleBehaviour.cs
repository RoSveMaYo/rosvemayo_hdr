﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleBehaviour : MonoBehaviour, ICollision
{
    GameObject GoICollidedWith;
    GameObject GoCollidedWithMe;

    Transform gooContainer;
    Rigidbody rb;
    SpringJoint springJoint;

    [Header("Physics Material")]
    [Range(0f, 1f)]
    public float dynamicFriction;
    [Range(0f, 1f)]
    public float bounciness;

    [Header("Spring Joint")]
    public float springForce;
    public float breakForce;

    bool collided = false;

    public void DoBehaviour()
    {
        //Get the collided Object
        GetOtherObject();

        //Set this objects tag to Sticky too.
        SetTagSticky();

        //Set this objects physic Material attributes.
        SetPhysicMaterial();

        //Set Parent of object.
        SetParent();

        //Set specific behaviour
        SetBehaviour();
    }

    void OnJointBreak(float breakForce)
    {
        DeParentMyself();
        DeParentObjectsConnectedWithMe();
    }

    void GetOtherObject()
    {
        CheckCollision cc = this.gameObject.GetComponent<CheckCollision>();
        GoICollidedWith = cc.GoICollidedWith;
    }

    void SetTagSticky()
    {
        //We add "Sticky" to our existing tag
        this.gameObject.tag = "Sticky" + this.gameObject.tag.ToString();
    }

    void SetPhysicMaterial()
    {
        //Set the bounciness and dynamic Friction of the objects physic Material
        PhysicMaterial phMat = this.gameObject.GetComponent<Collider>().material;
        phMat.bounciness = bounciness;
        phMat.dynamicFriction = dynamicFriction;
    }

    void SetParent()
    {
        //If the other object is the object to transport that has an object controller script...
        if (GoICollidedWith.transform.childCount != 0 && GoICollidedWith.transform.GetChild(0).tag.Contains("Container"))
        {
            this.transform.parent = GoICollidedWith.transform.GetChild(0);
        }

        //if we hit another stone...
        else
        {
            this.transform.parent = GoICollidedWith.transform.parent;
        }
    }

    void SetBehaviour()
    {
        this.gameObject.AddComponent<SpringJoint>();
        springJoint = this.gameObject.GetComponent<SpringJoint>();
        springJoint.connectedBody = GoICollidedWith.GetComponent<Rigidbody>();
        springJoint.spring = springForce;
        springJoint.maxDistance = 0.1f;
        springJoint.damper = 0.05f;
        springJoint.breakForce = breakForce;
        springJoint.breakTorque = breakForce;
    }

    void DeParentMyself()
    {
        this.transform.parent = null;
    }

    public void DeParentObjectsConnectedWithMe()
    {
        CheckCollision cc = this.gameObject.GetComponent<CheckCollision>();

        if(cc.GosCollidedWithMe != null)
        {
            foreach(GameObject go in cc.GosCollidedWithMe)
            {
                if(go != null)
                {
                    go.transform.parent = null;
                    go.GetComponent<BubbleBehaviour>().DeParentObjectsConnectedWithMe();

                    if (go.gameObject.GetComponent<SpringJoint>() != null)
                    {
                        SpringJoint spJoint = go.gameObject.GetComponent<SpringJoint>();
                        Destroy(spJoint);
                    }
                }

            }
        }
    }
}
