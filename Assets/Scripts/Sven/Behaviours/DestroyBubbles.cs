﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBubbles : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Contains("Goo"))
        {
            collision.gameObject.GetComponent<BubbleBehaviour>().DeParentObjectsConnectedWithMe();
            collision.gameObject.GetComponent<ExplosionBehaviour>().DoExplosion();
        }
    }
}
