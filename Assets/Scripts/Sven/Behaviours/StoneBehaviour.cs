﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneBehaviour : MonoBehaviour, ICollision
{
    GameObject other;
    Transform gooContainer;
    Rigidbody rb;

    [Header("Physics Material")]
    [Range(0f, 1f)]
    public float dynamicFriction;
    [Range(0f, 1f)]
    public float bounciness;

    bool collided = false;

    public void DoBehaviour()
    {
        //Get the collided Object
        GetOtherObject();

        //Check if the tag of the collided object contains the string "sticky"
        if(other.tag.Contains("Sticky"))
        {
            //Check if this object already collided with something
            if(!collided)
            {
                //Set this objects tag to Sticky too.
                SetTagSticky();

                //Set this objects physic Material attributes.
                SetPhysicMaterial();

                //Set Parent of object.
                SetParent();

                //Set specific behaviour
                SetBehaviour();
            }

            //The object collided, so we set the bool to true
            collided = true;
        }
    }

    void GetOtherObject()
    {
        //Get the registered object on collision from the CheckCollision script.
        CheckCollision cc = this.gameObject.GetComponent<CheckCollision>();
        other = cc.GoICollidedWith;
    }

    void SetTagSticky()
    {
        //We add "Sticky" to our existing tag
        this.gameObject.tag = "Sticky" + this.gameObject.tag.ToString();
    }

    void SetPhysicMaterial()
    {
        //Set the bounciness and dynamic Friction of the objects physic Material
        PhysicMaterial phMat = this.gameObject.GetComponent<Collider>().material;
        phMat.bounciness = bounciness;
        phMat.dynamicFriction = dynamicFriction;
    }

    void SetParent()
    {
        //If the other object is the object to transport that has an object controller script...
        if(other.transform.childCount != 0 && other.transform.GetChild(0).tag.Contains("Container"))
        {
            this.transform.parent = other.transform.GetChild(0);
        }

        //if we hit another stone...
        else
        {
            this.transform.parent = other.transform.parent;
        }
    }

    void SetBehaviour()
    {
        //Set the rigidbody to kinematic to get the static stick behaviour.
        rb = this.gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = true;
    }
}
