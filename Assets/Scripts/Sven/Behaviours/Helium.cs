﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helium : MonoBehaviour
{
    public float upForce;

    Rigidbody rb;

    private void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void FixedUpdate ()
    {
        rb.AddForce(Vector3.up * upForce, ForceMode.Force);
	}
}
