﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingMissileBehaviour : MonoBehaviour
{
    public GameObject target;
    public float homingSensitivity;
    public float velocityMultiplier;

    Rigidbody rb;
    bool touchedTarget = false;

    private void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (target != null && !touchedTarget)
        {
            rb.velocity = transform.forward * velocityMultiplier;

            Vector3 relativePosition = target.transform.position - this.transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(relativePosition);

            rb.MoveRotation(Quaternion.RotateTowards(this.transform.rotation, targetRotation, homingSensitivity));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject == target)
        {
            touchedTarget = true;
        }
    }
}
