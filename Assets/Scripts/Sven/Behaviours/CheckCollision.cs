﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CheckCollision : MonoBehaviour
{
    //List for all the scripts on the object that implement the ICollision Interface
    List<ICollision> _iCollisionList;

    //References the gameObject this gameObject collides with
    public GameObject GoICollidedWith;
    public List<GameObject> GosCollidedWithMe;

    bool collided = false;

    private void Start()
    {
        //initialize the List
        _iCollisionList = new List<ICollision>();

        //Find all scripts with the implemented Interface and store them in the List
        FindICollisionScripts();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Contains("Sticky"))
        {
            if(!collided)
            {
                //initialize the GameObject variable
                GoICollidedWith = collision.gameObject;

                if (collision.gameObject.GetComponent<CheckCollision>() != null)
                {
                    CheckCollision cc = collision.gameObject.GetComponent<CheckCollision>();
                    cc.GosCollidedWithMe.Add(this.gameObject);
                }

                //Execute the DoBehaviour() for all scripts with the ICollision Interface
                foreach (ICollision ic in _iCollisionList)
                {
                    ic.DoBehaviour();
                }
            }

            collided = true;
        }

    }

    void FindICollisionScripts()
    {
        var ss = this.gameObject.GetComponents<MonoBehaviour>().OfType<ICollision>();
        foreach (ICollision s in ss)
        {
            _iCollisionList.Add(s);
        }
    }
}
