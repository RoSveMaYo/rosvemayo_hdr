﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpandAtStart : MonoBehaviour
{
    public float startScale;
    public float endScale;
    public float lerpDuration;

    public bool random = false;

    Vector3 initialScale;

    private void Start()
    {
        SetStartScale();
        StartExpand();
    }

    public void DoBehaviour()
    {
        StartExpand();
    }

    void SetStartScale()
    {
        if(random)
        {
            startScale = Random.Range(0.25f, 0.5f);
        }

        initialScale = new Vector3(startScale, startScale, startScale);
        this.transform.localScale = initialScale;
    }

    IEnumerator IExpand(Vector3 start, Vector3 end, float time)
    {
        float i = 0f;
        float rate = (1f / time) * 1f;
        while(i < 1f)
        {
            i += Time.deltaTime * rate;
            this.transform.localScale = Vector3.Lerp(start, end, i);
            yield return null;
        }
    }

    void StartExpand()
    {
        if(random)
        {
            endScale = Random.Range(0.75f, 2f);
            lerpDuration = Random.Range(0.25f, 1f);
        }

        Vector3 desiredScale = new Vector3(endScale, endScale, endScale);
        StartCoroutine(IExpand(initialScale, desiredScale, lerpDuration));
    }
}
