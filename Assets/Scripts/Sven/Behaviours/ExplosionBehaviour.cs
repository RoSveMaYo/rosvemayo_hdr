﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBehaviour : MonoBehaviour
{
    [Header("Explosion")]
    public float explosionForce;
    public ParticleSystem explosion;
    public Vector3 normalVector;

    MaterialBehaviour mBehaviour;

    private void Start()
    {
        mBehaviour = this.gameObject.GetComponent<MaterialBehaviour>();
    }

    public void DoExplosion()
    {
        if(this.transform.parent != null)
        {
            Transform mainObject = this.transform.parent.transform.parent;
            Vector3 forceDir = (mainObject.position - this.transform.position).normalized;
            Rigidbody mainObjectRB = mainObject.gameObject.GetComponent<Rigidbody>();

            mainObjectRB.AddForceAtPosition(-normalVector.normalized * explosionForce, this.transform.position, ForceMode.Impulse);
            //mainObjectRB.AddForce(forceDir * explosionForce, ForceMode.Impulse);
            InstantiateParticleSystem();
            Destroy(this.gameObject);
        }

        else
        {
            Debug.Log("The Object you want to expode has no parent");
            Destroy(this.gameObject);
        }
    }

    void InstantiateParticleSystem()
    {
        explosion.GetComponent<ParticleSystemRenderer>().material = mBehaviour.endMaterial;
        ParticleSystem ps = Instantiate(explosion, this.gameObject.transform.position, Quaternion.identity);
    }
}
