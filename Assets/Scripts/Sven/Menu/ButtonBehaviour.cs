﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehaviour : MonoBehaviour
{
    public enum buttonFunction {Play, Quit, Restart, Exit, Menu};
    public buttonFunction selectedFunction;

    MenuSelection myMenuSelection;

    private void OnEnable()
    {
        myMenuSelection = FindObjectOfType<MenuSelection>();
    }

    public void DoButtonBehaviour(buttonFunction function)
    {
        switch(function)
        {
            case buttonFunction.Play:
                GameStateManager.instance.LoadScene(1);
                GameStateManager.instance.gameState = GameStateManager.States.LevelSelection;
                print("Play");
                break;

            case buttonFunction.Quit:
                Application.Quit();
                print("Quit");
                break;

            case buttonFunction.Restart:
                myMenuSelection.Resume();
                GameStateManager.instance.ReloadScene();
                InputManager.instance.setKeyBoardValues = false;
                InputManager.instance.setControllerValues = false;
                print("Restart");
                break;

            case buttonFunction.Exit:
                myMenuSelection.Resume();
                GameStateManager.instance.LoadScene(1);
                GameStateManager.instance.gameState = GameStateManager.States.LevelSelection;
                print("Exit");
                break;

            case buttonFunction.Menu:
                myMenuSelection.Resume();
                GameStateManager.instance.LoadScene(0);
                GameStateManager.instance.gameState = GameStateManager.States.Menu;
                print("Menu");
                break;

            default:
                Debug.Log("No Function selected");
                break;
        }
    }
}
