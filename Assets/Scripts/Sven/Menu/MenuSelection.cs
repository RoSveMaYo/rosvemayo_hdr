﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSelection : MonoBehaviour
{
    [Header("In correct order!")]
    public List<Button> buttons;
    public Button currentButton;

    public GameObject pauseMenuUI;

    bool pause;

    public int countDPad = 0;

    private void OnDisable()
    {
        InputManager.instance.menuDelegate -= HitButton;
        InputManager.instance.pauseMenuDelegate -= HitButton;
        InputManager.instance.pauseDelegate -= PauseBehaviour;
    }

    // Use this for initialization
    void Start ()
    {
        InputManager.instance.menuDelegate += HitButton;
        InputManager.instance.pauseMenuDelegate += HitButton;
        InputManager.instance.pauseDelegate += PauseBehaviour;

        currentButton = buttons[0];
        SelectButton(currentButton);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f || Input.GetKeyDown(KeyCode.UpArrow))
        {
            Up();
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f || Input.GetKeyDown(KeyCode.DownArrow))
        {
            Down();
        }

        if (Input.GetAxis("Vertical") > 0f)
        {
            if(countDPad < 1)
            {
                Up();
                countDPad++;
            }
        }

        if (Input.GetAxis("Vertical") < 0f)
        {
            if(countDPad < 1)
            {
                Down();
                countDPad++;
            }
        }

        if(Input.GetAxis("Vertical") == 0f)
        {
            countDPad = 0;
        }
    }

    void Up()
    {
        if(0 < buttons.IndexOf(currentButton))
        {
            DeselectButton(currentButton);
            currentButton = buttons[buttons.IndexOf(currentButton) - 1];
            SelectButton(currentButton);
        }
    }
    void Down()
    {
        if(buttons.Count - 1 > buttons.IndexOf(currentButton))
        {
            DeselectButton(currentButton);
            currentButton = buttons[buttons.IndexOf(currentButton) + 1];
            SelectButton(currentButton);
        }
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        pause = true;
        GameStateManager.instance.stateBeforePause = GameStateManager.instance.gameState;
        GameStateManager.instance.gameState = GameStateManager.States.Pause;
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        pause = false;
        GameStateManager.instance.gameState = GameStateManager.instance.stateBeforePause;
    }

    void PauseBehaviour()
    {
        if(!pause)
        {
            Pause();
        }

        else
        {
            Resume();
        }
    }

    
    void HitButton()
    {
        ButtonBehaviour bh = currentButton.GetComponent<ButtonBehaviour>();
        bh.DoButtonBehaviour(bh.selectedFunction);
    }
    
    /*
    void MenuBehaviour()
    {
        if (currentButton.name.Contains("Play"))
        {
            GameStateManager.instance.LoadScene(1);
            GameStateManager.instance.gameState = GameStateManager.States.LevelSelection;
        }

        if (currentButton.name.Contains("Quit"))
        {
            Application.Quit();
        }
    }

    void PauseMenuBehaviour()
    {
        if (currentButton.name.Contains("Restart"))
        {
            Resume();
            GameStateManager.instance.ReloadScene();
            InputManager.instance.setKeyBoardValues = false;
            InputManager.instance.setControllerValues = false;

        }

        if (currentButton.name.Contains("Exit"))
        {
            Resume();
            GameStateManager.instance.LoadScene(1);
            GameStateManager.instance.gameState = GameStateManager.States.LevelSelection;
        }

        if(currentButton.name.Contains("Menu"))
        {
            Resume();
            GameStateManager.instance.LoadScene(0);
            GameStateManager.instance.gameState = GameStateManager.States.Menu;
        }
    }
    */

    void SelectButton(Button button)
    {
        button.Select();
        Color highlightColor = button.colors.highlightedColor;
        highlightColor.a = 0.74f;

        button.transform.GetChild(0).GetComponent<Text>().color = highlightColor;
    }
    void DeselectButton(Button button)
    {
        Color deselectColor = button.colors.highlightedColor;
        deselectColor.a = 0.35f;

        button.transform.GetChild(0).GetComponent<Text>().color = deselectColor;
    }
}
