﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;

    public delegate void InputDelegate();

    public InputDelegate setValuesDelegate;
    public InputDelegate menuDelegate;
    public InputDelegate pauseMenuDelegate;
    public InputDelegate pauseDelegate;

    public InputDelegate setStoneToZero;

    //for shooting with diffrent inputs
    public InputDelegate keyboardShootingDelegate;
    public InputDelegate controllerShootingDelegate;

    //for switching with diffrent inputs
    public InputDelegate keyboardPrefabSwitchDelegate;
    public InputDelegate controllerPrefabSwitchDelegate;

    //triggers shot of dart
    public InputDelegate dartDelegate;

    //delegate for the gameManager
    public InputDelegate reloadDelegate;

    //to drop, clean up or switch the transport object
    public InputDelegate dropObjectDelegate;
    public InputDelegate cleanUpDelegate;
    public InputDelegate stopCleaningDelegate;
    public InputDelegate objectSwitchDelegate;

    [Header("Shooting")]
    public bool getButtonDown = false;
    float mouseDefaultFireRate = 0.1f;
    public float fireRate;
    float fireTime;
    float waitTilNextFire = 0;

    [Header("Current Prefab")]
    public int currentPrefab = 0;
    [HideInInspector]
    public int prefabLenght;

    [Header("GamePad")]
    public bool rightTrigger;
    public bool leftTrigger;
    public bool leftTriggerFirst;
    public bool rightTriggerFirst;
    //How much of the max Speed should be reached when only holding one trigger
    [Range(0.1f, 0.9f)]
    public float percentage;
    
    [Header("Connected Controller")]
    public string[] gamepads;
    public int Xbox_360_Controller = 0;

    public bool setControllerValues;
    public bool setKeyBoardValues;

    public bool switchJoysticks = false;

    //Check connected controllers and do singleton and dontdestroyonload
    void Awake ()
    {
        CheckConnectedControllers();

		if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        else if(instance != this)
        {
            Destroy(this.gameObject);
        }
	}

    //Check connected controllers in case of disconnect/reconnect. 
    //Handle the controller or keyboard input
    void Update ()
    {
        CheckConnectedControllers();

        if (Xbox_360_Controller == 1)
        {
            if(!setControllerValues)
            {
                setValuesDelegate();
                setControllerValues = true;
                setKeyBoardValues = false;
            }

            HandleControllerInput();
            waitTilNextFire -= Time.deltaTime / fireRate;
        }

        else
        {
            if(!setKeyBoardValues)
            {
                setValuesDelegate();
                setKeyBoardValues = true;
                setControllerValues = false;

                fireRate = mouseDefaultFireRate;
            }

            HandleMouseAndKeyboardInput();   
        }
	}

    //Updates the gamepad list
    void CheckConnectedControllers()
    {
        gamepads = Input.GetJoystickNames();

        for(int i = 0; i < gamepads.Length; i++)
        {
            if(gamepads[i].Contains("360") || gamepads[i].Contains("Controller"))
            {
                Xbox_360_Controller = 1;
            }

            else
            {
                Xbox_360_Controller = 0;
            }
        }
    }

    //ControllerInput: All Functions that go with the controller
    void HandleControllerInput()
    {
        if (GameStateManager.instance.gameState == GameStateManager.States.Playing)
        {
            TriggerShootController();
            ControllerMaterialSwitch();
            ControllerDropObject();
            ControllerCleanUp();
        }

        if (GameStateManager.instance.gameState != GameStateManager.States.Menu)
        {
            ControllerObjectSwitch();
        }

        if (GameStateManager.instance.gameState != GameStateManager.States.Playing)
        {
            ControllerLoadScene();
        }

        if (GameStateManager.instance.gameState != GameStateManager.States.Menu && GameStateManager.instance.gameState != GameStateManager.States.LevelSelection)
        {
            ControllerReloadScene();
        }
    }

    void TriggerShootController()
    {
        float rightTriggerInput = Input.GetAxis("RightTrigger");
        float leftTriggerInput = Input.GetAxis("LeftTrigger");

        SetControllerTriggerBools(rightTriggerInput, leftTriggerInput);
        SetControllerFireRate(rightTriggerInput, leftTriggerInput, percentage);
    }
    void SetControllerTriggerBools(float rightInput, float leftInput)
    {
        if (rightInput > 0)
        {
            rightTrigger = true;
        }

        else
        {
            rightTrigger = false;
        }

        if (leftInput > 0)
        {
            leftTrigger = true;
        }

        else
        {
            leftTrigger = false;
        }
    }
    void SetControllerFireRate(float rightInput, float leftInput, float multiplier)
    {
        if(rightTrigger && !leftTrigger)
        {
            rightTriggerFirst = true;
            leftTriggerFirst = false;

            float rightFireRate = 1 - (rightInput * multiplier);
            fireRate = rightFireRate;

            if(waitTilNextFire <= 0)
            {
                controllerShootingDelegate();
                waitTilNextFire = 1;
            }
        }

        if (leftTrigger && !rightTrigger)
        {
            leftTriggerFirst = true;
            rightTriggerFirst = false;

            float leftFireRate = 1 - (leftInput * multiplier);
            fireRate = leftFireRate;

            if (waitTilNextFire <= 0)
            {
                controllerShootingDelegate();
                waitTilNextFire = 1;
            }
        }

        if(rightTrigger && leftTrigger)
        {
            if(rightTriggerFirst)
            {
                float rightFireRate = 1 - (rightInput * multiplier);
                float updatedLeftInput = 1 - multiplier;
                float leftFireRate = leftInput * updatedLeftInput;

                float finalFireRate = rightFireRate - leftFireRate;
                fireRate = finalFireRate;

                if (waitTilNextFire <= 0)
                {
                    controllerShootingDelegate();
                    waitTilNextFire = 1;
                }
            }

            if(leftTriggerFirst)
            {
                float leftFireRate = 1 - (leftInput * multiplier);
                float updatedRightInput = 1 - multiplier;
                float rightFireRate = rightInput * updatedRightInput;

                float finalFireRate = leftFireRate - rightFireRate;
                fireRate = finalFireRate;

                if (waitTilNextFire <= 0)
                {
                    controllerShootingDelegate();
                    waitTilNextFire = 1;
                }
            }
        }
    }
    void ControllerMaterialSwitch()
    {
        if(Input.GetKeyDown("joystick button 5"))
        {
            if(currentPrefab < prefabLenght - 1)
            {
                currentPrefab++;
            }

            else if(currentPrefab == prefabLenght - 1)
            {
                currentPrefab = 0;
            }

            controllerPrefabSwitchDelegate();
        }

        if(Input.GetKeyDown("joystick button 4"))
        {
            if (currentPrefab > 0)
            {
                currentPrefab--;
            }

            else if (currentPrefab == 0)
            {
                currentPrefab = prefabLenght - 1;
            }

            controllerPrefabSwitchDelegate();
        }
    }
    void ControllerDropObject()
    {
        if(Input.GetKeyDown("joystick button 0"))
        {
            dropObjectDelegate();
            setStoneToZero();
        }
    }
    void ControllerLoadScene()
    {
        if(Input.GetKeyDown("joystick button 0"))
        {
            if (GameStateManager.instance.gameState == GameStateManager.States.LevelSelection)
            {
                GameStateManager.instance.LoadScene(GameStateManager.instance.selectedScene + 1);
                GameStateManager.instance.gameState = GameStateManager.States.Playing;
            }

            if (GameStateManager.instance.gameState == GameStateManager.States.Menu)
            {
                menuDelegate();
            }

            if(GameStateManager.instance.gameState == GameStateManager.States.Pause)
            {
                pauseMenuDelegate();
            }

            setKeyBoardValues = false;
            setControllerValues = false;
        }
    }
    void ControllerReloadScene()
    {
        if (Input.GetKeyDown("joystick button 7"))
        {
            pauseDelegate();
        }
    }
    void ControllerCleanUp()
    {
        if (Input.GetKey("joystick button 1"))
        {
            cleanUpDelegate();
        }
      
        else if(Input.GetKeyUp("joystick button 1"))
        {
            stopCleaningDelegate();
        } 
    }
    void ControllerObjectSwitch()
    {
        if (Input.GetKeyDown("joystick button 2") && !GameStateManager.instance.droppedObject)
        {
            objectSwitchDelegate();
        }
    }

    //Mouse & Keyboard Input: All Functions that go with the keyboard/mouse
    void HandleMouseAndKeyboardInput()
    {
        if(GameStateManager.instance.gameState == GameStateManager.States.Playing)
        {
            TriggerShoot();
            TriggerDart();
            TriggerMaterialSwitch();
            TriggerDrop();
            TriggerCleanUp();
        }

        if (GameStateManager.instance.gameState != GameStateManager.States.Menu)
        {
            TriggerObjectSwitch();
        }

        if (GameStateManager.instance.gameState != GameStateManager.States.Playing)
        {
            TriggerLoadScene();
        }

        if(GameStateManager.instance.gameState != GameStateManager.States.Menu && GameStateManager.instance.gameState != GameStateManager.States.LevelSelection)
        {
            TriggerReloadScene();
        }
    }

    void TriggerShoot()
    {
        if (getButtonDown)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                keyboardShootingDelegate();
            }
        }

        if (!getButtonDown)
        {
            if (Input.GetButton("Fire1") && Time.time > fireTime)
            {
                fireTime = Time.time + fireRate;
                keyboardShootingDelegate();
            }
        }
    }
    void TriggerDart()
    {
        if(Input.GetMouseButtonDown(2))
        {
            dartDelegate();
        }
    }
    void TriggerReloadScene()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pauseDelegate();
        }
    }
    void TriggerLoadScene()
    {
        if(Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.Return))
        {
            if (GameStateManager.instance.gameState == GameStateManager.States.LevelSelection)
            {
                GameStateManager.instance.LoadScene(GameStateManager.instance.selectedScene + 1);
                GameStateManager.instance.gameState = GameStateManager.States.Playing;
            }

            if (GameStateManager.instance.gameState == GameStateManager.States.Menu)
            {
                menuDelegate();
            }

            if(GameStateManager.instance.gameState == GameStateManager.States.Pause)
            {
                pauseMenuDelegate();
            }

            setKeyBoardValues = false;
            setControllerValues = false;
        }
    }
    void TriggerMaterialSwitch()
    {
        //Prefab1
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentPrefab = 0;
            keyboardPrefabSwitchDelegate();
        }

        //Prefab2
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentPrefab = 1;
            keyboardPrefabSwitchDelegate();
        }

        //Prefab3
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentPrefab = 2;
            keyboardPrefabSwitchDelegate();
        }

        //Prefab4
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            currentPrefab = 3;
            keyboardPrefabSwitchDelegate();
        }
    }
    void TriggerDrop()
    {
        if(Input.GetKeyDown(KeyCode.Alpha0) && !GameStateManager.instance.droppedObject)
        {
            dropObjectDelegate();
            setStoneToZero();
        }
    }
    void TriggerCleanUp()
    {
        if (Input.GetKey(KeyCode.Backspace))
        {
            cleanUpDelegate();
        }

        else if (Input.GetKeyUp(KeyCode.Backspace))
        {
            stopCleaningDelegate();
        }
    }
    void TriggerObjectSwitch()
    {
        if(Input.GetMouseButtonDown(1) && !GameStateManager.instance.droppedObject)
        {
            objectSwitchDelegate();
        }
    }
}
