﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    //Here we create an array for instances of our Sound class
    public Audio[] sounds;
    public AudioClip[] goalSounds;
    public AudioClip[] bubbleSounds;

    // Use this for initialization before Start()
    void Awake()
    {
        //We dont want that there can be multiple AudioManagers, whenever a new scene starts, so we use a Singleton pattern,
        //to check if there is already an instance of our AudioManager, and if yes, we just destroy it.
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if(instance != this)
        {
            Destroy(this.gameObject);
        }

        //Each instance of our Sound class will now get a AudioSource that is stored in the variable "source", we defined in our Sound class
        //We then set the diffrent variables of our Sound class equals to the variables the AudioClip brings with it.
        foreach (Audio s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    private void Start()
    {
        if(InputManager.instance != null)
        {
            InputManager.instance.keyboardShootingDelegate += PlayRandomBubbleSound;
        }

        else
        {
            Debug.Log("AudioManager cant find INPUTMANAGER");
        }
    }

    private void OnDisable()
    {
        InputManager.instance.keyboardShootingDelegate -= PlayRandomBubbleSound;
    }

    //This enables us to Play an AudioClip just through his name.
    //When using System; we can use the Array.Find Method.
    //We first define the array we want to look through.
    //Then we define a variable that refers to the element in the Sound array, which in our case is an instance of the Sound class.
    //We then want to find that sound which name is equal to the name given as an argument in the Play() method.
    //sound here is just a variable and can be named what ever you want, but .name is important, because we refer to the name of the sound in our Instance.
    public void Play(string name, bool changePitch)
    {
        Audio s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        if(changePitch)
        {
            float randomPitch = UnityEngine.Random.Range(0.5f, 1.5f);
            s.source.pitch = randomPitch;
        }

        s.source.Play();
    }

    public void PlayRandomBubbleSound()
    {
        if (bubbleSounds.Length == 0)
        {
            Debug.Log("NO DEATH SOUNDS IN ARRAY");
        }

        else
        {
            float maxElementIndex = bubbleSounds.Length - 1;
            float randomClipNumber = UnityEngine.Random.Range(0f, maxElementIndex);

            AudioClip randomClip = bubbleSounds[Mathf.RoundToInt(randomClipNumber)];
            Audio s = Array.Find(sounds, sound => sound.name == "Death");

            s.clip = randomClip;
            s.source.clip = s.clip;
            s.source.Play();
        }
    }

    public void SwitchSound(int levelIndex)
    {
        AudioClip activeClip = goalSounds[levelIndex - 1];
        Audio s = Array.Find(sounds, sound => sound.name == "Goal");
    
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
    
        s.clip = activeClip;
        s.source.clip = s.clip;
    }

}
