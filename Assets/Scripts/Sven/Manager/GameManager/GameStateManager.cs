﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager instance;

    public enum States {Menu, Pause, LevelSelection, Playing, Win, Lose};
    public States gameState;
    public States stateBeforePause;

    public bool droppedObject = false;

    public int selectedScene;

	// Use this for initialization
	void Awake ()
    {
		if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        else if(instance != this)
        {
            Destroy(this.gameObject);
        }
	}

    private void Start()
    {
        if (InputManager.instance != null)
        {
            InputManager.instance.reloadDelegate += ReloadScene;
            InputManager.instance.dropObjectDelegate += SetBool;
        }

        else
        {
            Debug.Log("GameStateManager cant find INPUTMANGER");
        }

        gameState = States.Menu;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            gameState = States.Playing;
        }
    }

    private void OnDisable()
    {
        InputManager.instance.reloadDelegate -= ReloadScene;
        InputManager.instance.dropObjectDelegate -= SetBool;
    }

    public void LoadScene(int level)
    {
        SceneManager.LoadScene(level, LoadSceneMode.Single);
        droppedObject = false;
        InputManager.instance.currentPrefab = 0;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        droppedObject = false;

        gameState = States.Playing;
        InputManager.instance.currentPrefab = 0;
    }

    void SetBool()
    {
        droppedObject = true;
    }
}
