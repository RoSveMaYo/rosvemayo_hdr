﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetVertices : MonoBehaviour
{
    Mesh mesh;
    Vector3[] vertices;

	// Use this for initialization
	void Start ()
    {
        mesh = this.gameObject.GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
	}
	
	// Update is called once per frame
	void Update ()
    {
        foreach(Vector3 v in vertices)
        {
            Vector3 worldPt = transform.TransformPoint(v);
            Debug.Log(worldPt);
        }

        
        // assign the local vertices array into the vertices array of the Mesh.
        mesh.vertices = vertices;
        mesh.RecalculateBounds();

    }
}
