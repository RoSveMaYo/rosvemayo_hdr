﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//If you have multiple materials, use multiple meshCombiners per Material
public class MeshCombiner : MonoBehaviour
{
    public int countFromElement;

    private void Awake()
    {
        CombineMeshes();
    }

    /*
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Alpha0))
            CombineMeshes();
	}
    */

    public void CombineMeshes()
    {
        Quaternion oldRot = this.transform.rotation;
        Vector3 oldPos = transform.position;

        this.transform.rotation = Quaternion.identity;
        this.transform.position = Vector3.zero;

        List<MeshFilter> meshFilterList = new List<MeshFilter>();
        MeshFilter[] mfilters;

        for (int i = countFromElement; i < this.gameObject.transform.childCount; i ++)
        {
            MeshFilter meshFilterComponent = this.gameObject.transform.GetChild(i).GetComponent<MeshFilter>();
            meshFilterList.Add(meshFilterComponent);
        }

        mfilters = meshFilterList.ToArray();

        Mesh finalMesh = new Mesh();
        CombineInstance[] combiners = new CombineInstance[mfilters.Length];

        for(int i = 0; i < mfilters.Length; i++)
        {
            combiners[i].subMeshIndex = 0;
            combiners[i].mesh = mfilters[i].sharedMesh;
            combiners[i].transform = mfilters[i].transform.localToWorldMatrix;
        }

        finalMesh.CombineMeshes(combiners);
        this.GetComponent<MeshFilter>().sharedMesh = finalMesh;

        this.transform.rotation = oldRot;
        this.transform.position = oldPos;

        for (int i = countFromElement; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
        }
    }

}
