﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBehaviour : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Contains("Goo"))
        {
            collision.gameObject.GetComponent<ExplosionBehaviour>().DoExplosion();
        }
    }
}
