﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseConditions : MonoBehaviour
{
    public enum Conditions {UnderWater, Timer, FlipOver};
    public Conditions loseCondition;

    public float timer;
    float startValue;
    bool count;

    public float coverPercentage;
    public float maxAngle;

	// Use this for initialization
	void Start ()
    {
        switch (loseCondition)
        {
            case Conditions.UnderWater:
                Debug.Log("Underwater");
                break;

            case Conditions.Timer:
                startValue = timer;
                count = true;
                break;

            case Conditions.FlipOver:
                Debug.Log("FlipOver");
                break;

            default:
                Debug.Log("No Condition");
                break;
        }        
	}

    void Update()
    {
        if(GameStateManager.instance.gameState == GameStateManager.States.Playing)
        {
            switch (loseCondition)
            {
                case Conditions.UnderWater:
                    Debug.Log("Underwater");
                    break;

                case Conditions.Timer:
                    Timer();
                    break;

                case Conditions.FlipOver:
                    Debug.Log("FlipOver");
                    break;

                default:
                    Debug.Log("No Condition");
                    break;
            }
        }

    }

    void Timer()
    {
        if (count && timer > 0.0f)
        {
            timer -= Time.deltaTime;
        }

        else if (timer <= 0.0f)
        {
            count = false;
            timer = startValue;
            GameStateManager.instance.gameState = GameStateManager.States.Lose;
            Debug.Log("YOU LOSE BECAUSE OF TIMER");
        }
    }
}
