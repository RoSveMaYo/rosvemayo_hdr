﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTrigger : MonoBehaviour
{
    //public GameObject objectHolder;
    public List<ParticleSystem> winParticles;

    //ObjectControllerScript objSwitch;
    public string currentObjectName;

    private void Start()
    {
        /*
        if(InputManager.instance != null)
        {
            InputManager.instance.objectSwitchDelegate += SwitchName;
        }

        else
        {
            Debug.Log("WinTrigger cant find INPUTMANAGER");
        }
        */

        //objSwitch = objectHolder.GetComponent<ObjectControllerScript>();
        ///SwitchName();
    }

    /*
    private void OnDisable()
    {
        InputManager.instance.objectSwitchDelegate -= SwitchName;
    }
    */

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.name.Equals(currentObjectName) && GameStateManager.instance.gameState != GameStateManager.States.Lose && GameStateManager.instance.gameState != GameStateManager.States.Win)
        {
            GameStateManager.instance.gameState = GameStateManager.States.Win;
            foreach(ParticleSystem ps in winParticles)
            {
                ps.Play();
            }
            Debug.Log("YOU WIN");
        }
    }

    /*
    void SwitchName()
    {
        currentObjectName = objSwitch.currentGameObject.transform.name;
    }
    */
}
