﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateCollider : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;

    public Vector2 colliderSize;

	// Use this for initialization
	void Start ()
    {
        float distance = (startPoint.position - endPoint.position).magnitude;
        float height = startPoint.localScale.y;
        float thickness = startPoint.localScale.z;

        BoxCollider bc = this.gameObject.AddComponent<BoxCollider>();
        bc.size = new Vector3(colliderSize.x, colliderSize.y, distance);

        bc.isTrigger = true;
	}
}
