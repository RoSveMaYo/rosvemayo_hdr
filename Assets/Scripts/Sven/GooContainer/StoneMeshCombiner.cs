﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//If you have multiple materials, use multiple meshCombiners per Material
public class StoneMeshCombiner : MonoBehaviour
{
    public int countFromElement;

    public float sumMass = 0f;

    Rigidbody parentRigidbody;
    MeshRenderer mr;
    MeshCollider finalMeshCollider;
    public Material meshMaterial;

    private void Start()
    {
        parentRigidbody = this.transform.parent.gameObject.GetComponent<Rigidbody>();
        mr = this.gameObject.GetComponent<MeshRenderer>();
        finalMeshCollider = this.gameObject.GetComponent<MeshCollider>();
    }

    public void CombineStoneMeshes()
    {
        //Store the initial position and rotation.
        Quaternion oldRot = this.transform.rotation;
        Vector3 oldPos = transform.position;

        this.transform.rotation = Quaternion.identity;
        this.transform.position = Vector3.zero;

        List<MeshFilter> meshFilterList = new List<MeshFilter>();
        List<GameObject> gameObjectList = new List<GameObject>();

        MeshFilter[] mfilters;

        //Get all meshfilters from the stone childs and add them to the list
        for (int i = countFromElement; i < this.gameObject.transform.childCount; i ++)
        {
            if(this.gameObject.transform.GetChild(i).tag.Contains("Stone"))
            {
                MeshFilter meshFilterComponent = this.gameObject.transform.GetChild(i).GetComponent<MeshFilter>();
                meshFilterList.Add(meshFilterComponent);
                gameObjectList.Add(this.gameObject.transform.GetChild(i).gameObject);
            }

            else
            {
                continue;
            }
        }
        //Convert list to array
        mfilters = meshFilterList.ToArray();

        //Get all the masses from the stone and add it to the parent rigidbody mass.
        foreach (GameObject go in gameObjectList)
        {
            sumMass += go.GetComponent<Rigidbody>().mass;
        }
        parentRigidbody.mass += sumMass;   


        Mesh finalMesh = new Mesh();
        CombineInstance[] combiners = new CombineInstance[mfilters.Length];

        for(int i = 0; i < mfilters.Length; i++)
        {
            combiners[i].subMeshIndex = 0;
            combiners[i].mesh = mfilters[i].sharedMesh;
            combiners[i].transform = mfilters[i].transform.localToWorldMatrix;
        }

        finalMesh.CombineMeshes(combiners);
        this.GetComponent<MeshFilter>().sharedMesh = finalMesh;
        finalMeshCollider.sharedMesh = finalMesh;
        mr.material = meshMaterial;

        this.transform.rotation = oldRot;
        this.transform.position = oldPos;

        for (int i = countFromElement; i < this.transform.childCount; i++)
        {
            if (this.gameObject.transform.GetChild(i).tag.Contains("Stone"))
                Destroy(this.transform.GetChild(i).gameObject);

            else
                this.gameObject.transform.GetChild(i).gameObject.GetComponent<SpringJoint>().connectedBody = this.transform.parent.gameObject.GetComponent<Rigidbody>();
        }
    }

}
