﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSwitch : MonoBehaviour
{
    public GameObject currentGameObject;

    int selectedChildObject = 0;
    public int level;
    int previousSelectedObject;

	// Use this for initialization
	void Awake ()
    {
        if(GameStateManager.instance.selectedScene != 0)
        {
            selectedChildObject = GameStateManager.instance.selectedScene - 1;
        }

        else if(GameStateManager.instance.selectedScene == 0)
        {
            selectedChildObject = GameStateManager.instance.selectedScene;
        }

        level = selectedChildObject + 1;

        SelectObject();
	}

    private void Start()
    {
        GameStateManager.instance.selectedScene = level;

        if(InputManager.instance != null)
        {
            InputManager.instance.objectSwitchDelegate += Switch;
        }

        else
        {
            Debug.Log("ObjectSwitch cant find INPUTMANAGER");
        }
    }

    private void OnDisable()
    {
        InputManager.instance.objectSwitchDelegate -= Switch;
    }

    void SelectObject()
    {
        int i = 0;
        foreach(Transform childObject in transform)
        {
            if(i == selectedChildObject)
            {
                childObject.gameObject.SetActive(true);
                currentGameObject = childObject.gameObject;
            }

            else
            {
                childObject.gameObject.SetActive(false);
            }

            i++;
        }
    }

    void SetSelected()
    {
        if (selectedChildObject >= transform.childCount - 1)
        {
            selectedChildObject = 0;
        }

        else
        {
            selectedChildObject++;
        }
        
        if (previousSelectedObject != selectedChildObject)
        {
            SelectObject();
        }

        level = selectedChildObject + 1;
        GameStateManager.instance.selectedScene = level;
    }

    void Switch()
    {
        previousSelectedObject = selectedChildObject;
        SetSelected();
    }
}
