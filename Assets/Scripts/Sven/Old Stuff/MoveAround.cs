﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAround : MonoBehaviour
{
    public Transform rotatePoint;
    public float angle;
	
	// Update is called once per frame
	void Update ()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 axisMovement = new Vector3(verticalInput, -horizontalInput, 0);

        this.transform.RotateAround(rotatePoint.position, axisMovement, angle * Time.deltaTime);
    }
}
