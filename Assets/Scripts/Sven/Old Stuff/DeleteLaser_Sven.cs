﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteLaser_Sven : MonoBehaviour
{
    public Transform firePoint;
    public LayerMask gooMask;

    LineRenderer lineRenderer;
    Vector3[] positions;

	// Use this for initialization
	void Start ()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
        //positions = new Vector3[lineRenderer.positionCount];
	}
	
	// Update is called once per frame
	void Update ()
    {
        RaycastHit gooHit;
        RaycastHit lineRendHit;
        
        if(Input.GetButton("Fire1"))
        {
            if (Physics.Raycast(firePoint.position, firePoint.forward, out lineRendHit))
            {
                lineRenderer.SetPosition(0, firePoint.position);
                lineRenderer.SetPosition(1, lineRendHit.point);
                //lineRenderer.GetPositions(positions);
            }

            if (Physics.Raycast(firePoint.position, firePoint.forward, out gooHit, 100f, gooMask.value))
            {
                Debug.Log(gooHit.transform.name);
                GameObject.Destroy(gooHit.transform.gameObject);
            }
        }

        else
        {
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.zero);
        }
	}
}
