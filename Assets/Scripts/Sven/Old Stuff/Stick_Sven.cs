﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick_Sven : MonoBehaviour
{
    Rigidbody rb;
    MeshRenderer mrend;
    Color gooColor;
    Collider bodyCollider;

    SpringJoint springJoint;

    [Header("Joint Mechanic")]
    public bool bubbleBehaviour;
    public bool stoneBehaviour;

    bool hasJoint = false;
    bool collided = false;
    bool collidedWithWater = false;

    bool hasParent = false;

    Vector3 contactPos;

    //To define how the prefab grows after being shot out to avoid clipping with the gun

    [Header("Quick Scale after shooting")]
    public float scaleFactor;
    public Vector3 minScale = new Vector3(0.05f, 0.05f, 0.05f);
    Vector3 prefabScale;
    float randomFloat;


    //Material Lerp

    [Header("Material Lerp")]
    public Material startMaterial;
    public Material endMaterial;
    [Range(0f, 50f)] public float materialLerpDuration;

    //Expand attributes on collision

    [Header("Expand on collision")]
    //public float growthSmoothing = 0.25f;
    public float expandTimeInDeciSeconds;
	public float expandFactor;
    public float maxSize;

    [HideInInspector]
	public bool expandOnlyOnce;
    [HideInInspector]
    public bool expandStrict;
    [HideInInspector]
    public bool expanded;

    //Color Lerp

    [Header("Color Lerp")]
    public bool lerpColor;
    [Range(0f, 1f)] public float colorDesaturationFactor;
	[Range(0f,0.5f)] public float minRGBValue;

	// Use this for initialization
	void Start ()
    {
        if(bubbleBehaviour)
        {
            stoneBehaviour = !bubbleBehaviour;
        }

        if(stoneBehaviour)
        {
            bubbleBehaviour = !stoneBehaviour;
        }

        prefabScale = this.transform.localScale;
        this.transform.localScale = minScale;

        randomFloat = Random.Range(1f, 2f);

        if(bodyCollider is SphereCollider)
        {
            SphereCollider test = this.GetComponent<SphereCollider>();
            test.radius = this.transform.localScale.x / 2;
        }
             
		rb = this.GetComponent<Rigidbody>();
        mrend = this.GetComponent<MeshRenderer>();
        mrend.material = startMaterial;
	}

    private void Update()
    {
        //Check if the connected Body has a parent, if not, own parent = null. Check if own connected body is missing, if yes, own parent = null
        if(springJoint != null)
        {
            if(springJoint.connectedBody != null)
            {
                if (springJoint.connectedBody.gameObject.transform.parent == null)
                {
                    this.transform.parent = null;
                }
            }

            else
            {
                this.transform.parent = null;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Water") && !collidedWithWater && !collided)
        {
            MaterialBlend();
            collidedWithWater = true;
        }
    }

    private void OnJointBreak(float breakForce)
    {
        this.transform.parent = null;
        //this.gameObject.tag = "Goo";
        //hasParent = false;
        //collided = false;
    }

    private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag.Contains("Sticky") /*&& !collided*/)
        {
            //Apply to all Materials
            //rb.velocity = Vector3.zero;
            if(!collided)
            {
                this.gameObject.tag = "Sticky" + this.gameObject.tag.ToString();
                PhysicMaterial pMaterial = this.gameObject.GetComponent<Collider>().material;
                pMaterial.bounciness = 0;

                if (!hasParent && other.transform.childCount != 0 && other.transform.GetChild(0).tag.Equals("GooContainer"))
                {
                    this.transform.parent = other.transform.GetChild(0);
                    hasParent = true;
                }

                else if (!hasParent && other.transform.childCount == 0)
                {
                    this.transform.parent = other.transform.parent;
                    hasParent = true;
                }
            }

            //Apply Goo Spring Joint Behaviour
            if(!hasJoint && bubbleBehaviour)
            {
                this.gameObject.AddComponent<SpringJoint>();
                springJoint = this.gameObject.GetComponent<SpringJoint>();
                springJoint.connectedBody = other.rigidbody;
                springJoint.spring = 40f;
                springJoint.maxDistance = 0.1f;
                springJoint.damper = 0.05f;
                springJoint.breakForce = 100f;
                springJoint.breakTorque = 100f;

                hasJoint = true;
            }

            
            if (!hasJoint && stoneBehaviour)
            {
                rb.isKinematic = true;
            }
            

            if (expandOnlyOnce)
            {
				if (!expanded)
                {
					Expand();
                    if(!collidedWithWater && !collided)
                        MaterialBlend();

					if(expandStrict)
                    {
						expanded = true;
					}
				}
			}

			else
            {
                Expand();
                if (!collidedWithWater && !collided)
                    MaterialBlend();
            }

            collided = true;

        }
    }

    void Expand()
    {
		StartCoroutine(IExpand());
	}

    void MaterialBlend()
    {
        StartCoroutine(IMaterialLerp());
    }

    IEnumerator IExpand()
    {
		for (float i = 0; i < expandTimeInDeciSeconds; i ++)
		{
            if(transform.localScale.x < randomFloat)
            {
                transform.localScale += new Vector3(expandFactor, expandFactor, expandFactor);
            }
            yield return null;
		}
        
		expanded = true;
	}

    IEnumerator IMaterialLerp()
    {
        for(int i = 0; i < materialLerpDuration; i++)
        {
            mrend.material.Lerp(startMaterial, endMaterial, i/materialLerpDuration);
            yield return null;

            if (lerpColor && mrend.material.color.r > minRGBValue)
            {
                gooColor = new Color(mrend.material.color.r - colorDesaturationFactor, mrend.material.color.g - colorDesaturationFactor, mrend.material.color.b - colorDesaturationFactor, mrend.material.color.a);
            }

            if(lerpColor)
                mrend.material.color = gooColor;
        }
    }
}
