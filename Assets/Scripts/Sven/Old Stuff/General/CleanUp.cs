﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanUp : MonoBehaviour
{
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Backspace))
        {
            foreach(Transform child in this.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
	}
}
