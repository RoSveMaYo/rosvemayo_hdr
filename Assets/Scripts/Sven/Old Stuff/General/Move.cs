﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    Rigidbody rb;
    public float JumpForce;

    public float speed;

    // Use this for initialization
    void Start ()
    {
        rb = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        WASD();
        Jump();
	}

    void WASD()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        transform.position += transform.forward * verticalInput *speed * Time.deltaTime;
        transform.position += transform.right * horizontalInput *speed * Time.deltaTime;
    }

    void Jump(){
        if (Input.GetButtonDown("Jump")){
            rb.AddForce(transform.up * JumpForce, ForceMode.Impulse);
        }
    }
}