﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
    private static bool created = false;

    // Use this for initialization
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "TestingEnvironment" && Input.GetKeyDown(KeyCode.RightArrow))
        {
            SceneManager.LoadScene("Basic Room", LoadSceneMode.Single);
        }

    }
}
