﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPhysics_Sven : MonoBehaviour {

	public float waterLevel = 4f;
	public float floatHeight = 2f;
	public float bounceDamp = 0.05f;
	public Vector3 buoyancyCentreOffset;

	private float forceFactor;
	private Vector3 actionPoint;
	private Vector3 upLift;

	private Rigidbody rigid;

	// Use this for initialization
	void Start ()
    {
		this.rigid = GetComponent<Rigidbody>();
	}
	
	private void OnTriggerStay(Collider other)
	{
        if(other.gameObject.tag.Equals("Water"))
        {
            BeLikeWater();
        }
    }

    void BeLikeWater()
    {
    	actionPoint = transform.position + transform.TransformDirection(buoyancyCentreOffset);
    	forceFactor = 1f - ((actionPoint.y - waterLevel)/ floatHeight);
    
    	if(forceFactor > 0f)
    	{	
    		Debug.Log("Force bigger 0");
    		upLift = -Physics.gravity * (forceFactor - this.rigid.velocity.y * bounceDamp);
    		this.rigid.AddForceAtPosition(upLift , actionPoint);
    	}
    }
}
