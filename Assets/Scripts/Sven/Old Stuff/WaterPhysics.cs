﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPhysics : MonoBehaviour
{
    public float force = 50f;
    public float reduceForceFactor = 2f;

    Collider waterCollider;

    private void Start()
    {
        waterCollider = this.gameObject.GetComponent<Collider>();
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
		
	}

    private void OnTriggerStay(Collider other)
    {
        Rigidbody otherRB = other.gameObject.GetComponent<Rigidbody>();

        Vector3 waterTopSurface = waterCollider.bounds.center + new Vector3(0, this.transform.localScale.y / 2, 0);
        Vector3 waterTopOffset = waterTopSurface - new Vector3(0, 1, 0);
        Vector3 otherCenter = other.bounds.center;

        float distanceToSurface = waterTopOffset.y - otherCenter.y;

        float waterForce;

        if(distanceToSurface > 2)
        {
            waterForce = force * distanceToSurface;
        }

        else
        {
            waterForce = 0f;
        }

        Debug.Log("waterForce: " + waterForce);
        otherRB.AddForce(Vector3.up * waterForce, ForceMode.Acceleration);
    }


}
