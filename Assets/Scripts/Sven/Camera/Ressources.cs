﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ressources : MonoBehaviour
{
    public int gooAmount;
    public int stoneAmount;
    public int heliumAmount;

    private void Start()
    {
        InputManager.instance.setStoneToZero += NullStone;
    }

    private void OnDisable()
    {
        InputManager.instance.setStoneToZero -= NullStone;
    }

    void NullStone()
    {
        stoneAmount = 0;
    }
}
