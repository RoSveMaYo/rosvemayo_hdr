﻿using System.Collections;
using UnityEngine;

public class MoveAroundCameraRig : MonoBehaviour
{
    Transform parentTransform;
    Vector3 updatedParentPosition;
    Transform cameraTransform;

    //stores the rotation of our pivot.
    Vector3 localRotation;
    public float cameraDistance = 10f;
    public float moveSpeed;
    public float resetSpeed;

    //Sensitivity
    [Header("Input Sensitivity")]
    public float mouseSensitivity = 4f;
    public float scrollSensitivity = 2f;

    //How long it takes for the camera, to reach its destination
    [Header("Damping")]
    public float orbitDamping = 10f;
    public float scrollDamping = 6f;

    [Header("Clamp Values")]
    public float minYAngle = 0f;
    public float maxYAngle = 90f;
    public float minDistanceToCamera = 1.5f;
    public float maxDistanceToCamera = 100f;

    float verticalInput;
    float horizontalInput;

    //whenever is false, camera controlls are normally
    public bool cameraDisable = false;

    private void OnDisable()
    {
        InputManager.instance.setValuesDelegate -= SetInputValues;
    }

    // Use this for initialization
    void Start ()
    {
        InputManager.instance.setValuesDelegate += SetInputValues;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cameraTransform = this.transform;
        parentTransform = this.transform.parent;
        updatedParentPosition = parentTransform.position;
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            InputManager.instance.switchJoysticks = !InputManager.instance.switchJoysticks;
        }

        //Move the parent
        if(InputManager.instance.Xbox_360_Controller == 1)
        {
            if(InputManager.instance.switchJoysticks)
            {
                verticalInput = -Input.GetAxis("Vertical");
                horizontalInput = -Input.GetAxis("Horizontal");
            }

            if(!InputManager.instance.switchJoysticks)
            {
                verticalInput = Input.GetAxis("RightStickYAxis");
                horizontalInput = Input.GetAxis("RightStickXAxis");
            }

            //Reset the parent
            if (Input.GetKey("joystick button 8"))
            {
                parentTransform.position = Vector3.Lerp(parentTransform.position, updatedParentPosition, Time.deltaTime * resetSpeed);
            }
        }

        else
        {
            verticalInput = Input.GetAxis("Vertical");
            horizontalInput = Input.GetAxis("Horizontal");

            //Reset the parent
            if (Input.GetKey(KeyCode.Space))
            {
                parentTransform.position = Vector3.Lerp(parentTransform.position, updatedParentPosition, Time.deltaTime * resetSpeed);
            }
        }

        parentTransform.position += ((parentTransform.up * verticalInput) + (parentTransform.right * horizontalInput)) *  moveSpeed * Time.deltaTime;
        updatedParentPosition = parentTransform.parent.transform.position;
    }

    // LateUpdate is called after Update. Camera needs to render, after everything else.
    void LateUpdate ()
    {
        //Toggle Camera Disable
		if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            cameraDisable = !cameraDisable;
        }

        if(!cameraDisable)
        {
            if(InputManager.instance.Xbox_360_Controller == 1)
            {
                if(!InputManager.instance.switchJoysticks)
                {
                    //Rotation of Camera based on Mouse Coordinates
                    if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                    {
                        if (GameStateManager.instance.gameState == GameStateManager.States.Menu)
                        {
                            localRotation.x += Input.GetAxis("Horizontal") * -mouseSensitivity; //For controller i had to invert the input to -mouseSensitivity
                            localRotation.y -= Input.GetAxis("Vertical") * -mouseSensitivity;
                        }

                        else
                        {
                            localRotation.x += Input.GetAxis("Horizontal") * mouseSensitivity;
                            localRotation.y -= Input.GetAxis("Vertical") * mouseSensitivity;
                        }


                        //Clamp the y Rotation to horizon not flipping over at the top
                        localRotation.y = Mathf.Clamp(localRotation.y, minYAngle, maxYAngle);
                    }

                    //Scrolling Input from our Mouse SCroll Wheel
                    if (Input.GetAxis("ArrowYAxis") != 0f)
                    {
                        float scrollAmount = Input.GetAxis("ArrowYAxis") * scrollSensitivity;

                        //0,3f is equal 30%. The Close we get, the less we zoom.
                        scrollAmount *= (cameraDistance * 0.2f);
                        cameraDistance += scrollAmount * -1f;

                        //Clamp Scroll to closest and furthest distance
                        cameraDistance = Mathf.Clamp(cameraDistance, minDistanceToCamera, maxDistanceToCamera);
                    }
                }

                if(InputManager.instance.switchJoysticks)
                {
                    //Rotation of Camera based on Mouse Coordinates
                    if (Input.GetAxis("RightStickXAxis") != 0 || Input.GetAxis("RightStickYAxis") != 0)
                    {
                        if (GameStateManager.instance.gameState == GameStateManager.States.Menu)
                        {
                            localRotation.x += Input.GetAxis("RightStickXAxis") * mouseSensitivity; //For controller i had to invert the input to -mouseSensitivity
                            localRotation.y -= Input.GetAxis("RightStickYAxis") * mouseSensitivity;
                        }

                        else
                        {
                            localRotation.x += Input.GetAxis("RightStickXAxis") * -mouseSensitivity;
                            localRotation.y -= Input.GetAxis("RightStickYAxis") * -mouseSensitivity;
                        }


                        //Clamp the y Rotation to horizon not flipping over at the top
                        localRotation.y = Mathf.Clamp(localRotation.y, minYAngle, maxYAngle);
                    }

                    //Scrolling Input from our Mouse SCroll Wheel
                    if (Input.GetAxis("ArrowYAxis") != 0f)
                    {
                        float scrollAmount = Input.GetAxis("ArrowYAxis") * scrollSensitivity;

                        //0,3f is equal 30%. The Close we get, the less we zoom.
                        scrollAmount *= (cameraDistance * 0.2f);
                        cameraDistance += scrollAmount * -1f;

                        //Clamp Scroll to closest and furthest distance
                        cameraDistance = Mathf.Clamp(cameraDistance, minDistanceToCamera, maxDistanceToCamera);
                    }
                }

            }

            else
            {
                //Rotation of Camera based on Mouse Coordinates
                if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                {
                    localRotation.x += Input.GetAxis("Mouse X") * mouseSensitivity; //For controller i had to invert the input to -mouseSensitivity
                    localRotation.y -= Input.GetAxis("Mouse Y") * mouseSensitivity;

                    //Clamp the y Rotation to horizon not flipping over at the top
                    localRotation.y = Mathf.Clamp(localRotation.y, minYAngle, maxYAngle);
                }

                //Scrolling Input from our Mouse SCroll Wheel
                if (Input.GetAxis("Mouse ScrollWheel") != 0f)
                {
                    float scrollAmount = Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity;

                    //0,3f is equal 30%. The Close we get, the less we zoom.
                    scrollAmount *= (cameraDistance * 0.3f);
                    cameraDistance += scrollAmount * -1f;

                    //Clamp Scroll to closest and furthest distance
                    cameraDistance = Mathf.Clamp(cameraDistance, minDistanceToCamera, maxDistanceToCamera);
                }
            }

        }

        //Actual Camera Rig Transformations
        Quaternion rotation = Quaternion.Euler(localRotation.y, localRotation.x, 0);
        parentTransform.rotation = Quaternion.Lerp(parentTransform.rotation, rotation, Time.deltaTime * orbitDamping);

        if(cameraTransform.localPosition.z != cameraDistance * -1f)
        {
            cameraTransform.localPosition = new Vector3(0f, 0f, Mathf.Lerp(cameraTransform.localPosition.z, cameraDistance * -1f, Time.deltaTime * scrollDamping));
        }
	}

    void SetInputValues()
    {
        if(InputManager.instance.Xbox_360_Controller == 1)
        {
            mouseSensitivity = 0.75f;
            scrollSensitivity = 0.08f;
            scrollDamping = 3f;
            moveSpeed = 5f;
        }

        else
        {
            mouseSensitivity = 4f;
            scrollSensitivity = 4f;
            scrollDamping = 6f;
            moveSpeed = 5f;
        }
    }
}
