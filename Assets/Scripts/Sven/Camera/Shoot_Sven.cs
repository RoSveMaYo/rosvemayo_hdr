﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot_Sven : MonoBehaviour
{
    [Header("Material Prefabs")]
	public GameObject[] gooPrefabs;

    [Header("Spawn Point")]
	public Transform gooSpawn;

    public GameObject currentGooPrefab;

    [Header("Dart")]
    public GameObject nerfDartPrefab;

    [Range(10f, 100f)]
    public float nerfVelocityMultiplier = 10f;

    [Header("ParticleSystem")]
    public ParticleSystem[] onPrefabSwitch;

    Ressources myRessources;
    public bool outOfRessources = false;

    /*
    [Header("Growth Mode")]
    public bool unlimitedGrowth;
    public bool growWhileGrowing;
    public bool limitedGrowth;
    */

    public LayerMask waterLayer;
    public LayerMask nerfDartLayer;
    public LayerMask gooLayer;

    private void OnDisable()
    {
        InputManager.instance.keyboardShootingDelegate -= Shooting;
        InputManager.instance.controllerShootingDelegate -= Shooting;

        InputManager.instance.dartDelegate -= ShootNerfDart;

        InputManager.instance.keyboardPrefabSwitchDelegate -= ChangePrefab;
        InputManager.instance.controllerPrefabSwitchDelegate -= ChangePrefab;
    }

    private void Start()
    {
        if(InputManager.instance != null)
        {
            InputManager.instance.setValuesDelegate += SetPrefabOnInputSwitch;

            InputManager.instance.keyboardShootingDelegate += Shooting;
            InputManager.instance.controllerShootingDelegate += Shooting;

            InputManager.instance.dartDelegate += ShootNerfDart;

            InputManager.instance.keyboardPrefabSwitchDelegate += ChangePrefab;
            InputManager.instance.controllerPrefabSwitchDelegate += ChangePrefab;
        }

        else
        {
            Debug.Log("Shoot_Sven cant find INPUTMANAGER");
        }

        InputManager.instance.prefabLenght = gooPrefabs.Length;
        myRessources = this.gameObject.GetComponent<Ressources>();

        //limitedGrowth = true;
        ChangePrefab();
    }

    void Shooting()
    {
        if(currentGooPrefab.transform.name != nerfDartPrefab.transform.name)
        {
            InstantiateGoo();
        }

        else
        {
            ShootNerfDart();
        }
    }

	void InstantiateGoo()
    {
        RaycastHit hit;

        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, Mathf.Infinity, gooLayer))
        {
            UseRessource(currentGooPrefab);
        }

        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, Mathf.Infinity, gooLayer) && !outOfRessources)
        {

            Quaternion randomRotation = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
            GameObject gooBullet = Instantiate(currentGooPrefab, hit.point, randomRotation);
            if(gooBullet.GetComponent<ExplosionBehaviour>() != null)
            {
                gooBullet.GetComponent<ExplosionBehaviour>().normalVector = hit.normal;
            }

            PlayBubbleSound();
        }           
	}

    void ShootNerfDart()
    {
        RaycastHit hit;

        if(Physics.Raycast(this.transform.position, this.transform.forward, out hit, Mathf.Infinity, nerfDartLayer))
        {
            GameObject nerfDart = Instantiate(nerfDartPrefab, gooSpawn.position, this.transform.parent.rotation);
            nerfDart.gameObject.GetComponent<HomingMissileBehaviour>().target = hit.collider.gameObject;
        }

        else
        {
            GameObject nerfDart = Instantiate(nerfDartPrefab, gooSpawn.position, this.transform.parent.rotation);
            Rigidbody nerfDartRigid = nerfDart.GetComponent<Rigidbody>();
            nerfDartRigid.velocity = this.transform.forward * nerfVelocityMultiplier;         
        }
    }

    void ChangePrefab()
    {
        currentGooPrefab = gooPrefabs[InputManager.instance.currentPrefab];
        onPrefabSwitch[InputManager.instance.currentPrefab].Play();
    }

    void PlayBubbleSound()
    {
        AudioManager.instance.Play("Bubble", true);
    }

    void UseRessource(GameObject currentPrefab)
    {
        string name = currentPrefab.transform.name;

        switch(name)
        {
            case "GooPrefab":
                if (myRessources.gooAmount > 0)
                {
                    outOfRessources = false;
                    myRessources.gooAmount -= 1;
                }

                else
                    outOfRessources = true;
                break;

            case "GooPrefab_Candy":
                if (myRessources.gooAmount > 0)
                {
                    outOfRessources = false;
                    myRessources.gooAmount -= 1;
                }

                else
                    outOfRessources = true;
                break;

            case "StonePrefab":
                if (myRessources.stoneAmount > 0)
                {
                    outOfRessources = false;
                    myRessources.stoneAmount -= 1;
                }

                else
                    outOfRessources = true;
                break;

            case "StonePrefab_Candy":
                if (myRessources.stoneAmount > 0)
                {
                    outOfRessources = false;
                    myRessources.stoneAmount -= 1;
                }

                else
                    outOfRessources = true;
                break;

            case "HeliumPrefab":
                if (myRessources.heliumAmount > 0)
                {
                    outOfRessources = false;
                    myRessources.heliumAmount -= 1;
                }

                else
                    outOfRessources = true;
                break;

            case "HeliumPrefab_Candy":
                if (myRessources.heliumAmount > 0)
                {
                    outOfRessources = false;
                    myRessources.heliumAmount -= 1;
                }

                else
                    outOfRessources = true;
                break;

            default:
                Debug.Log("No Prefabs found");
                break;
        }
    }

    void SetPrefabOnInputSwitch()
    {
        currentGooPrefab = gooPrefabs[0];
    }

    /*
    void CheckModi()
    {
        //Unlimited Growth
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            unlimitedGrowth = true;
            growWhileGrowing = false;
            limitedGrowth = false;
        }

        //Unlimited Growth while still growing
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            unlimitedGrowth = false;
            growWhileGrowing = true;
            limitedGrowth = false;
        }

        //Limited Growth
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            unlimitedGrowth = false;
            growWhileGrowing = false;
            limitedGrowth = true;
        }
    }
    */
}
