﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPhysics_OnWaterObject : MonoBehaviour
{
    [Header("Waypoints")]
    public Transform startPoint;
    public Transform endPoint;
    public float waterForce;
    public float pathForce = 15f;

    Vector3 waterforceDir;

    [Header("Water attributes")]
	public float waterLevel;
	public float floatHeight;
	public float bounceDamp = 0.05f;
	public Vector3 buoyancyCentreOffset;

    float startLevel;
    float tempLevel;

    [Header("Change Waterlevel over time")]
    public float frequency = 1f;
    public float amplitude = 0.5f;

    Vector3 gooActionPoint;
    Vector3 gooUpLift;
    float gooForceFactor;

    Vector3 waterPropActionPoint;
    Vector3 waterPropUpLift;
    float waterPropForceFactor;

    Collider waterCollider;

    [Header("Rigidbodies in water")]
    public List<GameObject> gooObjectsToFloat;
    public List<GameObject> waterPropsToFloat;

    public List<GameObject> objectsToMoveAlongPath;

    private void Start()
    {
        waterCollider = this.gameObject.GetComponent<Collider>();
        startLevel = waterLevel;

        if(startPoint != null && endPoint != null)
            waterforceDir = endPoint.position - startPoint.position;
    }

    private void Update()
    {
        UpdateWaterLevel();
    }
    
    //Add objects to lists
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Goo") || other.tag.Equals("StickyGoo"))
        {
            gooObjectsToFloat.Add(other.gameObject);
        }

        //ObjectToTransport Layer
        if (other.gameObject.layer == 12)
        {
            objectsToMoveAlongPath.Add(other.gameObject);
        }

        //WaterpropToFloat Layer
        if (other.gameObject.layer == 15)
        {
            waterPropsToFloat.Add(other.gameObject);
        }
    }

    //Remove objects from lists
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Goo") || other.tag.Equals("StickyGoo"))
        {
            gooObjectsToFloat.Remove(other.gameObject);
        }

        if (other.gameObject.layer == 12)
        {
            objectsToMoveAlongPath.Remove(other.gameObject);
        }

        if (other.gameObject.layer == 15)
        {
            waterPropsToFloat.Remove(other.gameObject);
        }
    }

    private void FixedUpdate()
    {
        FloatGooObjects();
        MoveObjectsAlongPath();
    }

    void UpdateWaterLevel()
    {
        tempLevel = startLevel;
        tempLevel += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        waterLevel = tempLevel;
    }

    //Give all goo type objects a buoancy and a force in direction of the path.
    void FloatGooObjects()
    {
        foreach (GameObject goo in gooObjectsToFloat)
        {
            if (goo == null)
                continue;

            Rigidbody rb = goo.GetComponent<Rigidbody>();

            gooActionPoint = goo.transform.position + goo.transform.TransformDirection(buoyancyCentreOffset);
            gooForceFactor = 1f - ((gooActionPoint.y - waterLevel) / floatHeight);

            if (gooForceFactor > 0f)
            {
                //Debug.Log("Force bigger 0");
                gooUpLift = -Physics.gravity * (gooForceFactor - rb.velocity.y * bounceDamp);
                rb.AddForceAtPosition(gooUpLift, gooActionPoint);
            }

            //Linear WaterForce
            rb.AddForce(waterforceDir.normalized * waterForce, ForceMode.Force);

            //Force along Path
            AddPathForce(goo, rb);
        }
    }

    //Move objects that are in water along the path
    void MoveObjectsAlongPath()
    {
        foreach(GameObject go in objectsToMoveAlongPath)
        {
            if (go == null)
                continue;

            Rigidbody rb = go.GetComponent<Rigidbody>();
            AddPathForce(go, rb);
        }
    }

    //Add force along a path
    void AddPathForce(GameObject gameOb, Rigidbody rigid)
    {
        if (PathHolder.instance != null && PathHolder.instance.pathEnabled)
        {
            if (PathHolder.instance.currentWaypointIndex < PathHolder.instance.pathObjects.Count)
            {
                Vector3 pathDir = PathHolder.instance.pathObjects[PathHolder.instance.currentWaypointIndex].transform.position - gameOb.transform.position;
                rigid.AddForce(pathDir.normalized * pathForce, ForceMode.Force);

                Debug.Log("AddingPathforce");
            }
        }
    }

    //Float Objects according to their vertices (WIP)
    void FloatWaterProp()
    {
        foreach(GameObject waterProp in waterPropsToFloat)
        {
            if (waterProp == null)
                continue;

            Rigidbody rb = waterProp.GetComponent<Rigidbody>();
            Mesh mesh = waterProp.GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;

            foreach(Vector3 v in vertices)
            {
                if (waterCollider.bounds.Contains(v))
                {
                    waterPropActionPoint = v + waterProp.transform.TransformDirection(buoyancyCentreOffset);
                    waterPropForceFactor = 1f - ((waterPropActionPoint.y - waterLevel) / floatHeight);

                    if (waterPropForceFactor > 0f)
                    {
                        //Debug.Log("Force bigger 0");
                        waterPropUpLift = -Physics.gravity * (waterPropForceFactor - rb.velocity.y * bounceDamp);
                        rb.AddForceAtPosition(waterPropUpLift, waterPropActionPoint);
                    }
                }
            }
        }
    }
}
