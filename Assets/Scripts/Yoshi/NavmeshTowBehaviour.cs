﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavmeshTowBehaviour : MonoBehaviour 
{
	public GameObject TowBoat;
	public Transform EndPoint;
	public float ForceMultiplier;
	private Vector3 _nextCorner;
	private Rigidbody _rb;
	private bool _coroutineRunning;
	RaycastHit hit;
	
	void Start () {
		_rb = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate () {
		if(GameStateManager.instance.droppedObject && EndPoint != null){
			if(!_coroutineRunning){
			StartCoroutine(IBlinkTowBoat());
			}
		}
		Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity);
	}

	IEnumerator IBlinkTowBoat(){
		_coroutineRunning = true;
		
		// if(hit.collider.tag.Equals("Water")){
			// print("rayhitwater");
			var towBoat = Instantiate(TowBoat, transform.position, Quaternion.identity);
			if(towBoat.GetComponent<NavmeshMoveTo>().goal == null){
				towBoat.GetComponent<NavmeshMoveTo>().goal = EndPoint;
			}
			yield return new WaitForSeconds(0.1f);
			if(towBoat.GetComponent<UnityEngine.AI.NavMeshAgent>().path.corners.Length >= 1){
				_nextCorner = towBoat.GetComponent<UnityEngine.AI.NavMeshAgent>().path.corners[1];
			}
			Destroy(towBoat);
		// }
		_coroutineRunning = false;
	}

	void PushTowardsCorner(){
		var dir = _nextCorner - transform.position;
		var normDir = dir.normalized;
		if(EndPoint != null){
			_rb.AddForce(normDir * ForceMultiplier, ForceMode.Force);
		}
	}

	private void OnTriggerStay(Collider other)
	{
		if(GetComponentsInChildren(typeof(BubbleBehaviour)).Length >= 1){
			//print("Watch me Jiggle");
			if(other.tag.Equals("Water")){
				PushTowardsCorner();
			}
		}
	}
}
