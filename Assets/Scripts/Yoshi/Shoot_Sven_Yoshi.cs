﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot_Sven_Yoshi : MonoBehaviour
{
	public GameObject[] gooPrefabs;
	//public GameObject gooContainer;
	public Transform gooSpawn;

    GameObject currentGooPrefab;

    public float initVel;
    public float fireRate;
    private float fireTime;

	public bool buttonDownMode;

    public bool unlimitedGrowth;
    public bool growWhileGrowing;
    public bool limitedGrowth;

    public float ExplosionForce;

    private int _layerMask = 1<<9;

    private void Start()
    {
        limitedGrowth = true;
        currentGooPrefab = gooPrefabs[0];
    }

    // Update is called once per frame
    void Update ()
    {
        Shoot();
        //CheckModi();
        ChangePrefab();
	}

	void InstantiateBullet()
    {
        RaycastHit hit;
        Quaternion randomRotation = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
		GameObject gooBullet = Instantiate(currentGooPrefab, gooSpawn.position, randomRotation);
        Stick_Sven_Yoshi stickScript = gooBullet.GetComponent<Stick_Sven_Yoshi>();

        if(unlimitedGrowth)
        {
            stickScript.expandOnlyOnce = false;
            stickScript.expandStrict = false;
        }

        if(growWhileGrowing)
        {
            stickScript.expandOnlyOnce = true;
            stickScript.expandStrict = false;
        }

        if(limitedGrowth)
        {
            stickScript.expandOnlyOnce = true;
            stickScript.expandStrict = true;
        }

        //gooBullet.GetComponent<Rigidbody>().velocity = this.transform.forward * initVel;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit))
        {
            gooBullet.transform.position = hit.point;
        }
	}

    void Shoot()
    {
        if (buttonDownMode)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                InstantiateBullet();
            }
        }

        //Middle Mouse button to make bubbles pop and add force to the object
        if(Input.GetMouseButtonDown(2)){
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, _layerMask)){
                print("rayhit");
                if (hit.collider.gameObject.tag.Equals("StickyGoo")){
                    print("goohit");
                    Transform cubeTrans = GameObject.Find("Cube").transform;
                    Vector3 forceDir = cubeTrans.position - hit.transform.position;
                    Vector3 forceDirNorm = forceDir.normalized;
                    Rigidbody parentRB = cubeTrans.gameObject.GetComponent<Rigidbody>();
                    parentRB.AddForce(forceDirNorm*ExplosionForce, ForceMode.Impulse);
                    Destroy(hit.collider.gameObject);
                }
            }
        }

        else if (!buttonDownMode)
        {
            if (Input.GetButton("Fire1") && Time.time > fireTime)
            {
                fireTime = Time.time + fireRate;
                InstantiateBullet();
            }
        }
    }

    void CheckModi()
    {
        //Unlimited Growth
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            unlimitedGrowth = true;
            growWhileGrowing = false;
            limitedGrowth = false;
        }

        //Unlimited Growth while still growing
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            unlimitedGrowth = false;
            growWhileGrowing = true;
            limitedGrowth = false;
        }

        //Limited Growth
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            unlimitedGrowth = false;
            growWhileGrowing = false;
            limitedGrowth = true;
        }
    }

    void ChangePrefab()
    {
        //Unlimited Growth
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentGooPrefab = gooPrefabs[0];
        }

        //Unlimited Growth while still growing
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentGooPrefab = gooPrefabs[1];
        }

        //Limited Growth
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentGooPrefab = gooPrefabs[2];
        }
    }
}
