﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick_Sven_Yoshi : MonoBehaviour
{
    Rigidbody rb;
    MeshRenderer mrend;
    Color gooColor;
    Collider bodyCollider;

    [Header("Joint Mechanic")]
    public bool addJoint = false;
    public bool singleJointPerObject = true;
    public bool bubbleBehaviour;
    public bool stoneBehaviour;

    bool hasJoint = false;
    bool canBecomeKinematic = true;
    bool collided = false;
    bool collidedWithWater = false;

    bool hasParent = false;


    //To define how the prefab grows after being shot out to avoid clipping with the gun

    [Header("Quick Scale after shooting")]
    public float scaleFactor;
    public Vector3 prefabScale;
    public Vector3 minScale = new Vector3(0.05f, 0.05f, 0.05f);

    //Material Lerp

    [Header("Material Lerp")]
    public Material startMaterial;
    public Material endMaterial;
    [Range(0f, 50f)] public float materialLerpDuration;

    //Expand attributes on collision

    [Header("Expand on collision")]
    //public float growthSmoothing = 0.25f;
    public int expandTimeInDeciSeconds;
	public float expandFactor;
    public float maxSize;

    int collisionCounter;

    [HideInInspector]
	public bool expandOnlyOnce;
    [HideInInspector]
    public bool expandStrict;
    [HideInInspector]
    public bool expanded;

    //Color Lerp

    [Header("Color Lerp")]
    public bool lerpColor;
    [Range(0f, 1f)] public float colorDesaturationFactor;
	[Range(0f,0.5f)] public float minRGBValue;

	// Use this for initialization
	void Start ()
    {
        collisionCounter = 0;
        prefabScale = this.transform.localScale;
        this.transform.localScale = minScale;

        if(bodyCollider is SphereCollider)
        {
            SphereCollider test = this.GetComponent<SphereCollider>();
            test.radius = this.transform.localScale.x / 2;
        }
             
		rb = this.GetComponent<Rigidbody>();
        mrend = this.GetComponent<MeshRenderer>();
        mrend.material = startMaterial;
	}

    void Update()
    {
        if(this.transform.localScale.x < prefabScale.x)
        {
            this.transform.localScale += new Vector3(scaleFactor, scaleFactor, scaleFactor) * Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Water") && !collidedWithWater && !collided)
        {
            MaterialBlend();
            collidedWithWater = true;
        }

        
        /*
        if (other.gameObject.tag.Equals("Goo"))
        {
            if(addJoint && !hasJoint && other.gameObject.GetComponent<Stick_Sven>().addJoint)
            {
                canBecomeKinematic = false;
                rb.isKinematic = false;

                rb.mass = 0.5f;

                ContactPoint contact = other.contacts[0];
                Vector3 contactPos = contact.point;
                rb.position = contactPos;

                this.gameObject.AddComponent<SpringJoint>();

                if(singleJointPerObject)
                    hasJoint = true;

                if (!singleJointPerObject)
                    hasJoint = false;

                SpringJoint spJoint = this.gameObject.GetComponent<SpringJoint>();
                spJoint.spring = 30f;
                Rigidbody otherRigid = other.gameObject.GetComponent<Rigidbody>();
                spJoint.connectedBody = otherRigid;
                MaterialBlend();
            }          
        }
        */
    }

    private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag.Contains("Sticky") && !collided)
        {
            //Apply to all Materials
            collided = true;
            //rb.velocity = Vector3.zero;
            this.gameObject.tag = "Sticky" + this.gameObject.tag.ToString();

            if (!hasParent && other.transform.childCount != 0 && other.transform.GetChild(0).tag.Equals("GooContainer"))
            {
                this.transform.parent = other.transform.GetChild(0);
                hasParent = true;
            }

            else if (!hasParent && other.transform.childCount == 0)
            {
                this.transform.parent = other.transform.parent;
                hasParent = true;
            }


            PhysicMaterial pMaterial = this.gameObject.GetComponent<Collider>().material;
            pMaterial.bounciness = 0;

            //Apply Goo Spring Joint Behaviour
            if(!hasJoint && bubbleBehaviour)
            {
                this.gameObject.AddComponent<SpringJoint>();
                SpringJoint springJoint = this.gameObject.GetComponent<SpringJoint>();
                springJoint.connectedBody = other.rigidbody;
                springJoint.spring = 40f;
                springJoint.maxDistance = 0.1f;
                springJoint.damper = 0.05f;

                hasJoint = true;
            }

            
            if (!hasJoint && stoneBehaviour)
            {
                Debug.Log("Stones are work in progress");

                
                // this.gameObject.AddComponent<FixedJoint>();
                // FixedJoint fxJoint = this.gameObject.GetComponent<FixedJoint>();
                // fxJoint.connectedBody = other.rigidbody;
                // fxJoint.enableCollision = false;

                // hasJoint = true;
                Destroy(rb);
                
            }
            

            if (expandOnlyOnce)
            {
				if (!expanded)
                {
					Expand();
                    if(!collidedWithWater)
                        MaterialBlend();

					if(expandStrict)
                    {
						expanded = true;
					}
				}
			}

			else
            {
                Expand();
                if (!collidedWithWater)
                    MaterialBlend();
            }
        }
    }

    void Expand()
    {
		StartCoroutine(IExpand());
	}

    void MaterialBlend()
    {
        StartCoroutine(IMaterialLerp());
    }

    IEnumerator IExpand()
    {
		for (int i = 0; i < expandTimeInDeciSeconds; i++)
		{
            if(transform.localScale.x < maxSize)
            {
                transform.localScale += new Vector3(expandFactor, expandFactor, expandFactor);
            }
            yield return null;
		}
		expanded = true;
        collisionCounter += 1;
	}

    IEnumerator IMaterialLerp()
    {
        for(int i = 0; i < materialLerpDuration; i++)
        {
            mrend.material.Lerp(startMaterial, endMaterial, i/materialLerpDuration);
            yield return null;

            if (lerpColor && mrend.material.color.r > minRGBValue)
            {
                gooColor = new Color(mrend.material.color.r - colorDesaturationFactor, mrend.material.color.g - colorDesaturationFactor, mrend.material.color.b - colorDesaturationFactor, mrend.material.color.a);
            }

            if(lerpColor)
                mrend.material.color = gooColor;
        }
    }
}
