﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot_Yoshi : MonoBehaviour
{
	public GameObject Goo;
	public GameObject GooContainer;
	public Transform GooSpawn;

	public float InitVel;
    public float fireRate;
    private float fireTime;

	public bool ButtonDown;
	
	// Update is called once per frame
	void Update ()
    {
		if (ButtonDown)
        {
			if (Input.GetMouseButtonDown(0))
            {
				Fire();
			}
		}

		else if (!ButtonDown)
        {
			if (Input.GetMouseButton(0) && Time.time > fireTime)
            {
                fireTime = Time.time + fireRate;
				Fire();
			}
		}
	}

	void Fire()
    {
		var _goo = Instantiate(Goo, GooSpawn.position, GooSpawn.rotation);
		_goo.transform.parent = GooContainer.transform;
		_goo.GetComponent<Rigidbody>().velocity = _goo.transform.forward * InitVel;
	}
}
