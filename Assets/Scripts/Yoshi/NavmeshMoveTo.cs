﻿    using UnityEngine;
    using System.Collections;
    
    public class NavmeshMoveTo : MonoBehaviour {
       
      public Transform goal;
	   public Vector3[] Corners;
	   public GameObject Cube;

	   public UnityEngine.AI.NavMeshAgent agent;
       
       void Start () {
          agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
          agent.destination = goal.position; 
       }

    /*
		void Update()
	   {
		   Corners = agent.path.corners;
		   if(Input.GetKeyDown(KeyCode.Space)){
			   foreach (var corner in agent.path.corners)
			   {
				   Instantiate(Cube, corner, Quaternion.identity);
			   }
		   }
	   }
       */
    }