﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPhysics_OnWaterObject_Yoshi : MonoBehaviour
{
    [Header("Waypoints")]
    public Transform startPoint;
    public Transform endPoint;
    public float waterForce;

    Vector3 waterforceDir;

    [Header("Water attributes")]
	public float waterLevel;
	public float floatHeight;
	public float bounceDamp = 0.05f;
	public Vector3 buoyancyCentreOffset;

    float startLevel;
    float tempLevel;

    [Header("Change Waterlevel over time")]
    public float frequency = 1f;
    public float amplitude = 0.5f;

	private float forceFactor;
	private Vector3 actionPoint;
	private Vector3 upLift;

    Collider waterCollider;

    [Header("Rigidbodies in water")]
    public List<GameObject> objectsToFloat;

    private void Start()
    {
        waterCollider = this.gameObject.GetComponent<Collider>();
        startLevel = waterLevel;

        if(startPoint != null && endPoint != null)
            waterforceDir = endPoint.position - startPoint.position;
    }

    private void Update()
    {
        UpdateWaterLevel();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Goo") || other.tag.Equals("StickyGoo"))
        {
            objectsToFloat.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Goo") || other.tag.Equals("StickyGoo"))
        {
            objectsToFloat.Remove(other.gameObject);
        }
    }

    private void FixedUpdate()
    {
        foreach(GameObject goo in objectsToFloat)
        {
            Rigidbody rb = goo.GetComponent<Rigidbody>();
            actionPoint = goo.transform.position + goo.transform.TransformDirection(buoyancyCentreOffset);
            forceFactor = 1f - ((actionPoint.y - waterLevel) / floatHeight);

            if (forceFactor > 0f)
            {
                //Debug.Log("Force bigger 0");
                upLift = -Physics.gravity * (forceFactor - rb.velocity.y * bounceDamp);
                rb.AddForceAtPosition(upLift, actionPoint);
            }

            rb.AddForce(waterforceDir.normalized * waterForce, ForceMode.Force);
        }
    }

    void UpdateWaterLevel()
    {
        tempLevel = startLevel;
        tempLevel += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        waterLevel = tempLevel;
    }
}
