﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavmeshTowBoat : MonoBehaviour 
{
	public GameObject TowBoat;
	public Transform EndPoint;
	public float WaitTime;
	public Vector3 Vel;
	public float Timer;
	public float ForceMultiplier;
	
	// Use this for initialization
	void Start () {
		Timer = WaitTime-0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		Timer += Time.deltaTime;
		if (Timer >= WaitTime){
			StartCoroutine(ISpawnTowBoat());
		}
		GetComponent<Rigidbody>().AddForce(Vel * ForceMultiplier, ForceMode.Force);
	}

	Vector3 SpawnTowBoat(){
		var towBoat = Instantiate(TowBoat, transform.position, Quaternion.identity);
		var vel = towBoat.GetComponent<UnityEngine.AI.NavMeshAgent>().velocity;
		print(vel);
		Destroy(towBoat);
		return vel;
	}

	IEnumerator ISpawnTowBoat(){
		var towBoat = Instantiate(TowBoat, transform.position, Quaternion.identity);
		towBoat.GetComponent<NavmeshMoveTo>().goal = EndPoint;
		var vel = towBoat.GetComponent<UnityEngine.AI.NavMeshAgent>().velocity;
		Vel = vel.normalized;
		print(Vel);
		yield return new WaitForFixedUpdate();
		Destroy(towBoat);
		Timer = 0;
	}
}
