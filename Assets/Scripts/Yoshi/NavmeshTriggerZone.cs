﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavmeshTriggerZone : MonoBehaviour {

	public Transform NewEndPoint;
	
	private void OnEnable()
	{
		GetComponent<MeshRenderer>().enabled = false;
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag.Contains("Sticky")){
			if(other.GetComponent<NavmeshTowBehaviour>() != null){
				other.GetComponent<NavmeshTowBehaviour>().EndPoint = NewEndPoint;
			}
		}
	}
}
