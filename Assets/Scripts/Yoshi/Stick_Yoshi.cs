﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick_Yoshi : MonoBehaviour {

	public int ExpandTimeInDeciSeconds;
	public float ExpandFactor;
	public bool ExpandOnlyOnce;
	public bool ExpandStrict;
	public float GooDesaturationFactor;
	[Range(0f,1f)] public float MinRGBValue;
	bool Expanded;
	Rigidbody RbRef;
	Material GooMat;
	Color _gooColour;
	// Use this for initialization
	void Start () {
		RbRef = GetComponent<Rigidbody>();
		GooMat = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
		//Unlimited Growth
		if (Input.GetKeyDown(KeyCode.Alpha1)){
			ExpandOnlyOnce = false;
			ExpandStrict = false;
		}
		//Unlimited Growth while still growing
		if (Input.GetKeyDown(KeyCode.Alpha2)){
			ExpandOnlyOnce = true;
			ExpandStrict = false;
		}
		//Limited Growth
		if (Input.GetKeyDown(KeyCode.Alpha3)){
			ExpandOnlyOnce = true;
			ExpandStrict = true;
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag.Contains("Sticky")){
			Destroy(RbRef);
			//RbRef.isKinematic = true;
			gameObject.tag = "StickyGoo";
			if (ExpandOnlyOnce){
				if (!Expanded){
					Expand();
					if(ExpandStrict){
						Expanded = true;
					}
				}
			}
			else{
				Expand();
			}
		}
	}

	void Expand(){
		StartCoroutine(IExpand());
	}

	IEnumerator IExpand(){
		for (int i = 0; i < ExpandTimeInDeciSeconds; i++)
		{
			transform.localScale += new Vector3 (ExpandFactor,ExpandFactor,ExpandFactor);
			yield return new WaitForSeconds(0.1f);
			//print(GooMat.color.r);
			if (GooMat.color.r >= MinRGBValue){
				_gooColour = new Color(GooMat.color.r - GooDesaturationFactor, GooMat.color.g - GooDesaturationFactor, GooMat.color.b - GooDesaturationFactor, GooMat.color.a);
			}
			GooMat.color = _gooColour;
		}
		Expanded = true;
	}
}
