﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick_Robi2 : MonoBehaviour
{
    Rigidbody rb;
    MeshRenderer mrend;
    Color gooColor;

    
    [Header("Joint Mechanic")]
    public bool addJoint = false;
    bool hasJoint = false;
    bool canGetJoint = true;
    bool canBecomeKinematic = true;
    

    //To define how the prefab grows after being shot out to avoid clipping with the gun

    [Header("Quick Scale after shooting")]
    public float scaleFactor;
    public Vector3 prefabScale;
    public Vector3 minScale = new Vector3(0.05f, 0.05f, 0.05f);

    //Material Lerp

    [Header("Material Lerp")]
    public Material startMaterial;
    public Material endMaterial;
    [Range(0f, 50f)] public float materialLerpDuration;

    //Expand attributes on collision

    [Header("Expand on collision")]
    //public float growthSmoothing = 0.25f;
    public int expandTimeInDeciSeconds;
	public float expandFactor;
    public float maxSize;

    int collisionCounter;

    [HideInInspector]
	public bool expandOnlyOnce;
    [HideInInspector]
    public bool expandStrict;
    [HideInInspector]
    public bool expanded;

    //Color Lerp

    [Header("Color Lerp")]
    public bool lerpColor;
    [Range(0f, 1f)] public float colorDesaturationFactor;
	[Range(0f,0.5f)] public float minRGBValue;

	// Use this for initialization
	void Start ()
    {
        collisionCounter = 0;
        prefabScale = this.transform.localScale;
        this.transform.localScale = minScale;

		rb = this.GetComponent<Rigidbody>();
        mrend = this.GetComponent<MeshRenderer>();
        mrend.material = startMaterial;
	}

    void Update()
    {
        if(this.transform.localScale.x < prefabScale.x)
        {
            this.transform.localScale += new Vector3(scaleFactor, scaleFactor, scaleFactor) * Time.deltaTime;
        }
    }

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag.Contains("Sticky"))
        {
            //Option to add SpringJoints.
            
            if(addJoint && other.gameObject.tag.Equals("WallSticky") && canBecomeKinematic)
            {
                canGetJoint = false;
                rb.isKinematic = true;
            }

            if(addJoint && !hasJoint && other.gameObject.GetComponent<Stick_Robi2>() != null && other.gameObject.GetComponent<Stick_Robi2>().addJoint && canGetJoint)
            {
                canBecomeKinematic = false;
                rb.isKinematic = false;

                rb.mass = 0.5f;

                ContactPoint contact = other.contacts[0];
                Vector3 contactPos = contact.point;
                rb.position = contactPos;

                this.gameObject.AddComponent<SpringJoint>();
                hasJoint = true;

                SpringJoint spJoint = this.gameObject.GetComponent<SpringJoint>();
                Rigidbody otherRigid = other.gameObject.GetComponent<Rigidbody>();
                spJoint.connectedBody = otherRigid;
            }

            else if(!addJoint)
            
            {
                PhysicMaterial pMaterial = this.gameObject.GetComponent<Collider>().material;
                pMaterial.bounciness = 0;

                rb.isKinematic = true;
            }

            gameObject.tag = "StickyGoo";

            if (expandOnlyOnce)
            {
				if (!expanded)
                {
					Expand();
                    MaterialBlend();
					if(expandStrict)
                    {
						expanded = true;
					}
				}
			}

			else
            {
				Expand();
                MaterialBlend();
            }
        }
    }

    void Expand()
    {
		StartCoroutine(IExpand());
	}

    void MaterialBlend()
    {
        StartCoroutine(IMaterialLerp());
    }

    IEnumerator IExpand()
    {
		for (int i = 0; i < expandTimeInDeciSeconds; i++)
		{
            if(transform.localScale.x < maxSize)
            {
                transform.localScale += new Vector3(expandFactor, expandFactor, expandFactor);
            }
            yield return null;
		}
		expanded = true;
        collisionCounter += 1;
	}

    IEnumerator IMaterialLerp()
    {
        for(int i = 0; i < materialLerpDuration; i++)
        {
            mrend.material.Lerp(startMaterial, endMaterial, i/materialLerpDuration);
            yield return null;

            if (lerpColor && mrend.material.color.r > minRGBValue)
            {
                gooColor = new Color(mrend.material.color.r - colorDesaturationFactor, mrend.material.color.g - colorDesaturationFactor, mrend.material.color.b - colorDesaturationFactor, mrend.material.color.a);
            }

            if(lerpColor)
                mrend.material.color = gooColor;
        }
    }
}
