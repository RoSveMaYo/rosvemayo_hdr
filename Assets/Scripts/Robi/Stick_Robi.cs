﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick_Robi : MonoBehaviour {

	public int ExpandTimeInDeciSeconds;
	public float ExpandFactor;
	public bool ExpandOnlyOnce;
	public bool ExpandStrict;
	public float GooDesaturationFactor;
	[Range(0f,1f)] public float MinRGBValue;
	bool Expanded;
	Rigidbody RbRef;
	Material GooMat;
	Color _gooColour;
	Color _gooEmissive;
	Renderer DefaultRenderer;
	Shader DefaultShader;

	//Material1 / StartMaterial
	public Material MaterialStart;
	//Material2 / GoalMaterial
	public Material MaterialEnd;
	//Lerp Duration
	[Range(0f, 50f)]public float LerpDuration;
	//ParticleSystem
	ParticleSystem PSystem;
	// Use this for initialization
	void Start () {
		RbRef = GetComponent<Rigidbody>();
		GooMat = GetComponent<MeshRenderer>().material;
		DefaultShader = GetComponent<MeshRenderer>().material.shader;
		DefaultRenderer = GetComponent<Renderer>();
		DefaultRenderer.material = MaterialStart;
		PSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		//Unlimited Growth
		if (Input.GetKeyDown(KeyCode.Alpha1)){
			ExpandOnlyOnce = false;
			ExpandStrict = false;
		}
		//Unlimited Growth while still growing
		if (Input.GetKeyDown(KeyCode.Alpha2)){
			ExpandOnlyOnce = true;
			ExpandStrict = false;
		}
		//Limited Growth
		if (Input.GetKeyDown(KeyCode.Alpha3)){
			ExpandOnlyOnce = true;
			ExpandStrict = true;
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		PSystem.Play();
		if (other.gameObject.tag.Contains("Sticky")){
			//Destroy(RbRef);
			RbRef.isKinematic = true;
			gameObject.tag = "StickyGoo";
			if (ExpandOnlyOnce){
				if (!Expanded){
					Expand();
					MaterialBlend();
					if(ExpandStrict){
						Expanded = true;
					}
				}
			}
			else{
				Expand();
				MaterialBlend();
			}
		}
	}

	void Expand(){
		StartCoroutine(IExpand());
	}

	void MaterialBlend()
	{
		StartCoroutine(MaterialBlending());
	}

	IEnumerator IExpand(){
		for (int i = 0; i < ExpandTimeInDeciSeconds; i++)
		{
			transform.localScale += new Vector3 (ExpandFactor,ExpandFactor,ExpandFactor);
			yield return new WaitForSeconds(0.1f);
			//print(GooMat.color.r);
			if (GooMat.color.r >= MinRGBValue){
				//_gooColour = new Color(GooMat.color.r - GooDesaturationFactor, GooMat.color.g - GooDesaturationFactor, GooMat.color.b - GooDesaturationFactor, GooMat.color.a);
				/* _gooEmissive = new Color(GooMat.GetColor("_EmissionColor").r - GooDesaturationFactor , GooMat.GetColor("_EmissionColor").g - GooDesaturationFactor  , GooMat.GetColor("_EmissionColor").b - GooDesaturationFactor , GooMat.GetColor("_EmissionColor").a - GooDesaturationFactor);
				//DynamicGI.SetEmissive(DefaultRenderer, new Color(1f, 0.1f, 0.5f, 1.0f) * GooDesaturationFactor);
				DefaultRenderer.UpdateGIMaterials(); */
				//DefaultRenderer.material.Lerp(MaterialStart , MaterialEnd , i / ExpandTimeInDeciSeconds);
			}
			
			//GooMat.color = _gooColour;
			//GooMat.SetColor("_Color" , _gooEmissive);
		}
		Expanded = true;
	}

	IEnumerator MaterialBlending()
	{
		
		for (int i = 0 ; i < LerpDuration; i++)
		{
			DefaultRenderer.material.Lerp(MaterialStart , MaterialEnd , i/LerpDuration);
			yield return new WaitForSeconds(0.1f);
		}
		
	}
}
