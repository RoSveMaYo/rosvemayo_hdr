﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLevel : MonoBehaviour {

	[Range(-5f,5f)]public float RotationSpeed = 2.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Rotate();
	}

	void Rotate()
	{

		this.transform.Rotate(new Vector3(0f,0f,RotationSpeed));
	}
}
