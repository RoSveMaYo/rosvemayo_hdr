﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMeshCollider : MonoBehaviour {

    SkinnedMeshRenderer meshRenderer;
    MeshCollider pinaCollider;

    public void UpdateCollider()
    {
        Mesh colliderMesh = new Mesh();
        meshRenderer.BakeMesh(colliderMesh);
        pinaCollider.sharedMesh = null;
        pinaCollider.sharedMesh = colliderMesh;
    }

    // Use this for initialization
    void Start () {
        meshRenderer = GetComponent<SkinnedMeshRenderer>();
        pinaCollider = GetComponent<MeshCollider>();
        UpdateCollider();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
