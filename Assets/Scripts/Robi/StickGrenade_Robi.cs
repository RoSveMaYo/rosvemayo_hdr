﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickGrenade_Robi : MonoBehaviour
{
    Rigidbody rb;
    MeshRenderer mrend;
    Color gooColor;

    public Material startMaterial;
    public Material endMaterial;
    [Range(0f, 50f)] public float materialLerpDuration;

    public int expandTimeInDeciSeconds;
	public float expandFactor;
    public float maxSize;

    [HideInInspector]
	public bool expandOnlyOnce;
    [HideInInspector]
    public bool expandStrict;
    [HideInInspector]
    public bool expanded;
    //public bool hadContact;
    //public Vector3 contactOffset = new Vector3(0.1f, 0.1f, 0.1f);

    public bool lerpColor;
    [Range(0f, 1f)] public float colorDesaturationFactor;
	[Range(0f,0.5f)] public float minRGBValue;

    public GameObject SubInstance;
    List<GameObject> SubInstanceArray;
    bool clustered = false;
	// Use this for initialization
	void Start ()
    {
		rb = this.GetComponent<Rigidbody>();
        mrend = this.GetComponent<MeshRenderer>();
        mrend.material = startMaterial;
        SubInstanceArray = new List<GameObject>();
        SubInstanceArray.Add(SubInstance);
        SubInstanceArray.Add(SubInstance);
        SubInstanceArray.Add(SubInstance);
        SubInstanceArray.Add(SubInstance);
        SubInstanceArray.Add(SubInstance);
	}

	private void OnCollisionEnter(Collision other)
	{
        
		if (other.gameObject.tag.Contains("Sticky"))
        {
            PhysicMaterial pMaterial = this.gameObject.GetComponent<SphereCollider>().material;
            pMaterial.bounciness = 0;

            /*
            ContactPoint contact = other.contacts[0];
            Vector3 contactPos = contact.point + contactOffset;

            if(!hadContact)
                rb.position = contactPos;

            hadContact = true;
            */
            
			rb.isKinematic = true;
			gameObject.tag = "StickyGoo";

			if (expandOnlyOnce)
            {
				if (!expanded)
                {
					Expand();
                    MaterialBlend();
                    SpawnSubClusters();
                    this.clustered = true;
					if(expandStrict)
                    {
						expanded = true;
					}
				}
			}

			else
            {
				Expand();
                MaterialBlend();
                SpawnSubClusters();
                this.clustered = true;
            }
        }
	}

	void Expand()
    {
		StartCoroutine(IExpand());
	}

    void MaterialBlend()
    {
        StartCoroutine(IMaterialLerp());
    }

    void SpawnSubClusters()
    {
        StartCoroutine(GrenadeSpawn());
    }

    IEnumerator IExpand()
    {
		for (int i = 0; i < expandTimeInDeciSeconds; i++)
		{
            if(transform.localScale.x < maxSize)
            {
                transform.localScale += new Vector3(expandFactor, expandFactor, expandFactor);
            }
            yield return new WaitForSeconds(0.1f);
		}
		expanded = true;
	}

    IEnumerator IMaterialLerp()
    {
        for(int i = 0; i < materialLerpDuration; i++)
        {
            mrend.material.Lerp(startMaterial, endMaterial, i/materialLerpDuration);
            yield return new WaitForSeconds(0.1f);

            if (lerpColor && mrend.material.color.r > minRGBValue)
            {
                gooColor = new Color(mrend.material.color.r - colorDesaturationFactor, mrend.material.color.g - colorDesaturationFactor, mrend.material.color.b - colorDesaturationFactor, mrend.material.color.a);
            }

            if(lerpColor)
                mrend.material.color = gooColor;
        }
    }
    IEnumerator GrenadeSpawn()
    {   
        if(!clustered)
        {
            foreach (GameObject obj in SubInstanceArray)
                {
                    
                        Vector3 randomDirection = new Vector3(Random.Range(-1f,1f), 0.25f , Random.Range(-1f,1f));
                        GameObject ClustX = Instantiate(obj , this.transform.position + randomDirection , Quaternion.identity );
                        Rigidbody ClustXRigid = ClustX.GetComponent<Rigidbody>();
                        ClustXRigid.AddForce(new Vector3 (Random.Range(-200f,300f), Random.Range(100f,200f), Random.Range(-200f,300f)) );
                        yield return null;
                    
                }
        }
    }

}
